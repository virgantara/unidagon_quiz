<?php

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use app\models\SimakMasterprogramstudi;
use app\models\SimakMasterdosen;
use app\models\SimakJadwal;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\jui\AutoComplete;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jadwal';
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : $tahun_id;
$dosen = !empty($_GET['dosen']) ? $_GET['dosen'] : $dosen;
$nama_dosen = !empty($_GET['nama_dosen']) ? $_GET['nama_dosen'] : $nama_dosen;
?>

<h3 class="page-title">Jadwal</h3>
<div class="row">
  <div class="col-md-6 col-lg-6 col-xs-12">
     <?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/dosen'],
      'options' => [
            'id' => 'form_validation',
      ]
    ]); ?>
     <div class="form-group">
          <label class="control-label ">Tahun Akademik</label>
          <?= Html::dropDownList('tahun_id',$tahun_id,\yii\helpers\ArrayHelper::map($listTahun,'tahun_id','nama_tahun'),['class'=>'form-control','id'=>'tahun_id']) ?>

          
          
      </div>
      <?php 

      if(Yii::$app->user->can('sekretearis') || Yii::$app->user->can('baak'))
      {
        if(Yii::$app->user->can('sekretearis') && !Yii::$app->user->can('baak'))
        {
          $listProdi =ArrayHelper::map(SimakMasterprogramstudi::find()->where(['kode_prodi' => Yii::$app->user->identity->prodi])->all(),'kode_prodi','nama_prodi');
        }

        else{
          $listProdi =ArrayHelper::map(SimakMasterprogramstudi::find()->all(),'kode_prodi','nama_prodi');
          $listDosen = \yii\helpers\ArrayHelper::map(\app\models\SimakMasterdosen::find()->orderBy(['nama_dosen'=>SORT_ASC])->all(),'id','nama_dosen');
        }
        
      ?>
      <div class="form-group">
          <label class="control-label ">Prodi</label>
           <?= Select2::widget([
            'name' => 'prodi',
            'value' => !empty($_GET['prodi']) ? $_GET['prodi'] : '',
            'data' => $listProdi,
            'options'=>['id'=>'prodi_id','placeholder'=>Yii::t('app','- Pilih Prodi -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>

      </div>
      <div class="form-group">
          <label class="control-label ">Dosen</label>
          
          <?php
          echo DepDrop::widget([
              'name' => 'dosen',      
              'data' => $listDosen,
              'value' => !empty($_GET['dosen']) ? $_GET['dosen'] : '',
              'type'=>DepDrop::TYPE_SELECT2,
              'options'=>['id'=>'dosen'],
              'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
              'pluginOptions'=>[
                  'depends'=>['tahun_id','prodi_id'],
                  'initialize' => true,
                  'placeholder'=>'- Pilih Dosen -',
                  'url'=>Url::to(['/simak-masterdosen/subdosen'])
              ]
            ]);
          ?>
          
      </div>
      <?php 
    }


      ?>
      <div class="form-group clearfix">
        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
        <a target="_blank" class="btn btn-success" href="<?=Url::to(['simak-jadwal/export','tahun'=>$tahun_id,'did'=>$dosen]);?>"><i class="fa fa-file-pdf-o"></i> Download Jadwal</a>
      </div>
    
     <?php ActiveForm::end(); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Jadwal</h3>
      </div>
      <div class="panel-body ">
        <?php 
$tglnow = date('Y-m-d');
$is_allowed = $tglnow >= $tahun_akademik_aktif->nilai_mulai && $tglnow <= $tahun_akademik_aktif->nilai_selesai;
if(!$is_allowed){
?>
    <div class="alert alert-info">
      <i class="fa fa-warning"></i> Mohon maaf, jadwal input nilai sudah ditutup
    </div>
  <?php }?>
   
        <div class="table-responsive">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>#</th>
                <th>Kelas</th>
                <th>Kampus</th>
                <th>Mata Kuliah</th>
                <th>SKS</th>
                <th>Hari</th>
                <th>Jam</th>
                <th>Prodi</th>
                <th>Jml Mhs</th>
                <th class="text-center">Opsi</th>
                
              </tr>
            </thead>
            <tbody>
              <?php 
              $i = 0;

              $prodi = Yii::$app->user->can('sekretearis') || Yii::$app->user->can('Dosen') ? Yii::$app->user->identity->prodi : $_GET['prodi'];
              foreach($results as $q => $m)
              {
                $mk = \app\models\SimakMastermatakuliah::find()->where([
                  'kode_prodi' => $prodi,
                  'tahun_akademik' => $_GET['tahun_id'],
                  'kode_mata_kuliah' => $m->kode_mk
                ])->one();

                $kampus = \app\models\SimakKampus::find()->where(['kode_kampus' => $m->kampus])->one();
                $i++;
              ?>
              <tr>
                <td><?=$i;?></td>
                <td><?=$m->kelas;?></td>
                <td><?=$kampus->nama_kampus;?></td>
                <td>[<?=$m->kode_mk;?>] <?=$mk->nama_mata_kuliah;?></td>
                <td><?=$mk->sks;?></td>
                <td><?=$m->hari;?></td>
                <td><?=$m->jam;?></td>
                <td><?=$mk->kodeProdi->nama_prodi;?></td>
                <td>
                  <?=$total_mhs[$m->id];?>
                </td>
                <td>

                    <?= Html::a('<i class="fa fa-edit"></i> Nilai', ['simak-datakrs/nilai','id'=>$m->id,'tahun'=>$tahun_id],['class'=>'btn btn-primary']) ?>
                    <?php 
                    $tglNow = date('Y-m-d');
                    if($tglnow >= $tahun_akademik_aktif->nilai_mulai && $tglnow <= $tahun_akademik_aktif->nilai_selesai){
                   ?>
                   
                    <?= Html::a('<i class="fa fa-file"></i> Bobot Materi', ['simak-jadwal/bobot','id'=>$m->id,'tahun'=>$tahun_id],['class'=>'btn btn-primary']) ?>
                    <?php 
                  }
                    ?>
                    <?= Html::a('<i class="fa fa-users"></i> Absensi', ['simak-datakrs/absensi','id'=>$m->id,'tahun'=>$tahun_id],['class'=>'btn btn-primary']) ?>
                   
                </td>
              </tr>
              <?php 
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
   </div>
</div>
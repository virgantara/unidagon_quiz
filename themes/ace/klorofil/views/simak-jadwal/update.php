<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SimakJadwal */

$this->title = 'Update Simak Jadwal: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Simak Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                	<?= Html::encode($this->title) ?>
                </h2>
            </div>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    	</div>
    </div>

</div>

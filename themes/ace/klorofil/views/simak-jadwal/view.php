<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SimakJadwal */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Simak Jadwals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <?= Html::encode($this->title) ?>
                </h2>
            </div>

        <div class="body">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>


    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'hari',
            'jam',
            'kode_mk',
            'kode_dosen',
            'semester',
            'kelas',
            'fakultas',
            'prodi',
            'kd_ruangan',
            'tahun_akademik',
            'kuota_kelas',
            'kampus',
            'presensi:ntext',
            'materi',
            'bobot_formatif',
            'bobot_uts',
            'bobot_uas',
            'bobot_harian1',
            'bobot_harian',
            'jadwal_temp_id',
            'created_at',
            'updated_at',
        ],
    ]) ?>

        </div>
    </div>

</div>
<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SimakJadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nilai per Kelas';
$this->params['breadcrumbs'][] = $this->title;

$tahun_id = !empty($_GET['tahun_id']) ? $_GET['tahun_id'] : $tahun_id;
$dosen = !empty($_GET['dosen']) ? $_GET['dosen'] : $dosen;
$nama_dosen = !empty($_GET['nama_dosen']) ? $_GET['nama_dosen'] : $nama_dosen;

$tmp = \app\models\SimakMasterprogramstudi::find();

if(Yii::$app->user->can('sekretearis') && !Yii::$app->user->can('theCreator'))
{
  $tmp->andWhere(['kode_prodi'=>Yii::$app->user->identity->prodi]);
}

$semester = !empty($_GET['semester']) ? $_GET['semester'] : $semester;
$listProdi = $tmp->all();
$prodi = !empty($_GET['prodi']) ? $_GET['prodi'] : '';
$kode_mk = !empty($_GET['kode_mk']) ? $_GET['kode_mk'] : '';
$jadwal_id = !empty($_GET['jadwal_id']) ? $_GET['jadwal_id'] : '';
?>

<h3 class="page-title"><?=$this->title;?></h3>
<div class="row">
  <div class="col-md-6 col-lg-6 col-xs-12">
     <?php $form = ActiveForm::begin([
      'method' => 'GET',
      'action' => ['simak-jadwal/nilai'],
      'options' => [
            'id' => 'form_validation',
      ]
    ]); ?>
     <div class="form-group">
          <label class="control-label ">Tahun Akademik</label>
          <?= Html::dropDownList('tahun_id',$tahun_id,\yii\helpers\ArrayHelper::map($listTahun,'tahun_id','nama_tahun'),['class'=>'form-control','id'=>'tahun_id']) ?>
      </div>
   
      <div class="form-group">
          <label class="control-label ">Prodi</label>
           <?= Select2::widget([
            'name' => 'prodi',
            'value' => $prodi,
            'data' => \yii\helpers\ArrayHelper::map($listProdi,'kode_prodi','nama_prodi'),
            'options'=>[
              'id'=>'prodi_id',
              'placeholder'=>Yii::t('app','- Pilih Prodi -'),

            ],
            'pluginOptions' => [
              'initialize' => true,
                'allowClear' => true,
            ],
        ]) ?>
         
      </div>
      <div class="form-group">
          <label class="control-label ">Semester</label>
          <?= Html::dropDownList('semester',$semester,\app\helpers\MyHelper::getListSemester(),['id'=>'semester','class'=>'form-control','prompt'=>'- Pilih Semester -']) ?>
      </div>
      <div class="form-group">
            <label class="control-label ">Mata kuliah</label>
            <?= DepDrop::widget([
                    'name' => 'kode_mk',
                    'value' => $kode_mk,
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'kode_mk'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['prodi_id','tahun_id','semester'],
                        'initialize' => true,
                        'placeholder'=>'- Pilih Mata Kuliah -',
                        'url'=>Url::to(['/simak-mastermatakuliah/submatkul'])
                    ]
                ]) ?>

            
        </div>
      <div class="form-group">
            <label class="control-label ">Kelas</label>
            <?= DepDrop::widget([
                    'name' => 'jadwal_id',
                    'value' =>$jadwal_id,
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'jadwal_id'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['prodi_id','tahun_id','semester','kode_mk'],
                        'initialize' => true,
                        'placeholder'=>'- Pilih Kelas -',
                        'url'=>Url::to(['/simak-jadwal/subjadwal'])
                    ]
                ]) ?>

            
        </div>      
      <div class="form-group clearfix">
        <button type="submit" class="btn btn-primary" name="btn-cari" value="1"><i class="fa fa-search"></i> Cari</button>
        <a target="_blank" class="btn btn-success" href="<?=Url::to(['simak-jadwal/export-nilai','id'=>$jadwal_id]);?>"><i class="fa fa-file-pdf-o"></i> Download Rekap Nilai per Kelas</a>
      </div>
    
     <?php ActiveForm::end(); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h3 class="panel-title">Jadwal</h3>

      </div>
      <div class="panel-body ">

<?php 
$tglnow = date('Y-m-d');
$is_allowed = $tglnow >= $tahun_akademik_aktif->nilai_mulai && $tglnow <= $tahun_akademik_aktif->nilai_selesai;

if($is_allowed)
{
  echo Html::a('<i class="fa fa-edit"></i> Update Bobot',['simak-jadwal/bobot','id'=>$jadwal->id],['class'=>'btn btn-info','target'=>'_blank']);
}

if(!$is_allowed){
?>
    <div class="alert alert-info">
      <i class="fa fa-warning"></i> Mohon maaf, jadwal input nilai sudah ditutup
    </div>
  <?php }?>
        <div class="table-responsive">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>#</th>
                <th>NIM</th>
                <th>Mahasiswa</th>
                <th>Semester</th>
                <th>Harian<br>(<?=$jadwal->bobot_harian;?> %)</th>
                <th>Normatif<br>(<?=$jadwal->bobot_formatif;?> %)</th>
                <th>UTS<br>(<?=$jadwal->bobot_uts;?> %)</th>
                <th>UAS<br>(<?=$jadwal->bobot_uas;?> %)</th>
                <th>NA</th>
                <th>NH</th>
                 <?php 
                    $tglNow = date('Y-m-d');
                    if($tglnow >= $tahun_akademik_aktif->nilai_mulai && $tglnow <= $tahun_akademik_aktif->nilai_selesai){
                   ?>
                <th>Opsi</th>
              <?php }?>
              </tr>
            </thead>
            <tbody>
              <?php 
              foreach($results as $q => $m)
              {
                $presisi = 0.00001;
                $nilai_angka = round($m->nilai_angka, 2);
                $nilai_huruf = "";
                for($i = 0;$i < count($range_nilai);$i++){
                  $rn = $range_nilai[$i];
                  
                  if(($nilai_angka - $rn["min"]) > $presisi && ($nilai_angka - $rn["max"]) <= $presisi){
                    $nilai_huruf = $rn["val"];
                    break;
                  }
                }
              ?>
              <tr>
                <td><?=$q+1;?></td>
                <td><?=$m->mahasiswa0->nim_mhs;?></td>
                <td><?=$m->mahasiswa0->nama_mahasiswa;?></td>
                <td><?=$m->mahasiswa0->semester;?></td>
                <td><?=$m->harian;?></td>
                <td><?=$m->normatif;?></td>
                <td><?=$m->uts;?></td>
                <td><?=$m->uas;?></td>
                <td><?=$m->nilai_angka;?></td>
                <td><?=$m->nilai_huruf;?></td>
                 <?php 
                    if($is_allowed){
                   ?>
                <td><?=Html::a('<i class="fa fa-edit"></i> Update Nilai',['simak-datakrs/update-nilai','id'=>$m->id],['class'=>'btn btn-info']);?></td>
              <?php }?>
              </tr>
              <?php 
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
   </div>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SimakJadwal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="body">

    <?php $form = ActiveForm::begin([
    	'options' => [
            'id' => 'form_validation',
    	]
    ]); ?>

        <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'hari',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Hari</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'jam',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Jam</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'kode_mk',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Kode mk</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'kode_dosen',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Kode dosen</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'semester',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Semester</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'kelas',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Kelas</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'fakultas',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Fakultas</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'prodi',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Prodi</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'kd_ruangan',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Kd ruangan</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'tahun_akademik',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Tahun akademik</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'kuota_kelas',['options' => ['tag' => false]])->textInput()->label(false) ?>

            <label class="form-label">Kuota kelas</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'kampus',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Kampus</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'presensi',['options' => ['tag' => false]])->textarea(['rows' => 6])->label(false) ?>

            <label class="form-label">Presensi</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'materi',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Materi</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'bobot_formatif',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Bobot formatif</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'bobot_uts',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Bobot uts</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'bobot_uas',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Bobot uas</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'bobot_harian1',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Bobot harian1</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'bobot_harian',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true,'required'=>'required'])->label(false) ?>

            <label class="form-label">Bobot harian</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'jadwal_temp_id',['options' => ['tag' => false]])->textInput()->label(false) ?>

            <label class="form-label">Jadwal temp</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'created_at',['options' => ['tag' => false]])->textInput()->label(false) ?>

            <label class="form-label">Created at</label>
            </div>
        </div>
                <div class="form-group form-float">
            <div class="form-line">
            <?= $form->field($model, 'updated_at',['options' => ['tag' => false]])->textInput()->label(false) ?>

            <label class="form-label">Updated at</label>
            </div>
        </div>
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>
    
    <?php ActiveForm::end(); ?>

</div>

<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

$this->title = Yii::t('app', 'Profil User') . ': ' . $model->username;
?>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $user app\models\User */
/* @var $form yii\widgets\ActiveForm */



?>
<div class="panel panel-profile">
    <div class="clearfix">
        <!-- LEFT COLUMN -->
        <div class="profile-left">
            <!-- PROFILE HEADER -->
            <div class="profile-header">
                <div class="overlay"></div>
                <div class="profile-main">
                    <i class="lnr lnr-user"></i>
                    <h3 class="name"><?=$mhs->nama_mahasiswa;?></h3>
                   
                </div>
                
            </div>
            <!-- END PROFILE HEADER -->
            <!-- PROFILE DETAIL -->
            <div class="profile-detail">
                <div class="profile-info">
                     <?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
      echo '<div class="flash alert alert-' . $key . '">' . $message . '<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button></div>';
    }
    ?>
   
                    <h4 class="heading">Basic Info</h4>
                    <ul class="list-unstyled list-justify">
                         <li>NIM <span><?=$mhs->nim_mhs;?></span></li>
                        <li>Tempat, Tanggal Lahir <span><?=$mhs->tempat_lahir;?>, <?=$mhs->tgl_lahir;?></span></li>
                        <li>NIK/NO KTP <span><?=$mhs->ktp;?></span></li>
                        <li>JK <span><?=$mhs->jenis_kelamin;?></span></li>
                        <li>HP <span><?=$mhs->hp;?></span></li>
                        <li>Email <span><?=$model->email;?></span></li>
                       
                    </ul>
                </div>
                
                <div class="profile-info">
                    <h4 class="heading">Address Info</h4>
                    <ul class="list-unstyled list-justify">
                        
                        <li>Jalan/Alamat <span><?=$mhs->alamat;?></span></li>
                        <li>RT/RW <span><?=$mhs->rt.' / '.$mhs->rw;?></span></li>
                        <li>Dusun <span><?=$mhs->dusun;?></span></li>
                        <li>Desa <span><?=$mhs->desa;?></span></li>
                        <li>Kecamatan <span><?=$mhs->kecamatan;?></span></li>
                        <li>Kab/Kota <span><?php
                        $kabupaten = \app\models\SimakKabupaten::find()->where(['id'=>$mhs->kabupaten])->one();
                        echo !empty($kabupaten) ? $kabupaten->kab : '';

                        ?></span></li>
                        <li>Provinsi <span>
                            <?php
                        $provinsi = \app\models\SimakPropinsi::find()->where(['id'=>$mhs->provinsi])->one();
                        echo !empty($provinsi) ? $provinsi->prov : '';

                        ?>
                        </span></li>
                    </ul>
                </div>

                <div class="profile-info">
                    <h4 class="heading">About</h4>
                    <p><!-- Interactively fashion excellent information after distinctive outsourcing. --></p>
                </div>
                <div class="text-center">
                	<a href="<?=Url::to(['simak-mastermahasiswa/update-profil']);?>" class="btn btn-primary"><i class="fa fa-user"></i> Edit Profil</a>
                	<a href="<?=Url::to(['user/ubah-akun']);?>" class="btn btn-primary"><i class="fa fa-lock"></i> Ganti Password</a>
                </div>
            </div>
            <!-- END PROFILE DETAIL -->
        </div>
        <!-- END LEFT COLUMN -->
        <!-- RIGHT COLUMN -->
      
        <!-- END RIGHT COLUMN -->
    </div>
</div>


<?php
use app\rbac\models\AuthItem;
use kartik\password\PasswordInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $user app\models\User */
/* @var $form yii\widgets\ActiveForm */
$nama_mahasiswa = '';

if('Mahasiswa' == $user->item_name)
{
  $mhs = \app\models\SimakMastermahasiswa::find()->where(['nim_mhs'=>$user->nim])->one();

  $nama_mahasiswa = !empty($mhs) ? $mhs->nama_mahasiswa : '';

}

?>
<div class="user-form">

    <?php $form = ActiveForm::begin(['id' => 'form-user']); ?>

        <?= $form->field($user, 'username')->textInput(
                ['placeholder' => Yii::t('app', 'Create username'), 'autofocus' => true]) ?>

        <?= $form->field($user, 'display_name')->textInput(
                ['placeholder' => Yii::t('app', 'Create Display Name')]) ?>
        
        <?= $form->field($user, 'email')->input('email', ['placeholder' => Yii::t('app', 'Enter e-mail')]) ?>

        <?php if ($user->scenario === 'create'): ?>

            <?= $form->field($user, 'password')->widget(PasswordInput::classname(), 
                ['options' => ['placeholder' => Yii::t('app', 'Create password')]]) ?>

        <?php else: ?>

            <?= $form->field($user, 'password')->widget(PasswordInput::classname(),
                     ['options' => ['placeholder' => Yii::t('app', 'Change password ( if you want )')]]) ?> 

        <?php endif ?>

    <div class="row">
    <div class="col-md-6">

        <?= $form->field($user, 'status')->dropDownList($user->statusList) ?>

        <?php foreach (AuthItem::getRoles() as $item_name): ?>
            <?php $roles[$item_name->name] = $item_name->name ?>
        <?php endforeach ?>
        <?= $form->field($user, 'item_name')->widget(Select2::classname(), [
            'data' => $roles,

            'options'=>['placeholder'=>Yii::t('app','- Pilih Role -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])?>
       
        <?= $form->field($user, 'kampus')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(\app\models\SimakKampus::find()->all(),'kode_kampus','nama_kampus'),

            'options'=>['id'=>'kampus_id','placeholder'=>Yii::t('app','- Pilih Kampus -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])?>
       <?= $form->field($user, 'fakultas')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(\app\models\SimakMasterfakultas::find()->orderBy(['nama_fakultas'=>SORT_ASC])->all(),'kode_fakultas','nama_fakultas'),

            'options'=>['id'=>'fakultas_id','placeholder'=>Yii::t('app','- Pilih Fakultas -')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])?>
        <?= $form->field($user, 'prodi')->widget(DepDrop::classname(), [
            'type'=>DepDrop::TYPE_SELECT2,
            'options'=>['id'=>'prodi_id'],
            'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
            'pluginOptions'=>[
                'depends'=>['fakultas_id'],
                'initialize' => true,
                'placeholder'=>'- Pilih Prodi -',
                'url'=>Url::to(['/simak-masterprogramstudi/subprodi'])
            ]
        ]) ?>
        
        <div class="form-group">
            <label>Dosen</label>
         <?php
          echo DepDrop::widget([
              'name' => 'dosen',   
              'type'=>DepDrop::TYPE_SELECT2,
              'options'=>['id'=>'dosen'],
              'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
              'pluginOptions'=>[
                  'depends'=>['prodi_id'],
                  'initialize' => true,
                  'placeholder'=>'- Pilih Dosen -',
                  'url'=>Url::to(['/simak-masterdosen/subdosenprodi'])
              ]
            ]);
          ?>
          </div>
          <div class="form-group">
            <label>Cari Mahasiswa</label>
             <?= Html::textInput('nama_mahasiswa',$nama_mahasiswa,['class'=>'form-control','id'=>'nama_mahasiswa']) ?>
                <?php 
                        AutoComplete::widget([
                'name' => 'nama_mahasiswa',
                'id' => 'nama_mahasiswa',
                'clientOptions' => [
                'source' => Url::to(['simak-mastermahasiswa/ajax-cari-mahasiswa']),
                'autoFill'=>true,
                'minLength'=>'3',
                'select' => new JsExpression("function( event, ui ) {
                    $('#nim').val(ui.item.nim);
                 }")],
                'options' => [
                    'class' => 'form-control'
                ]
             ]); ?>
          </div>
          <div class="form-group">
            <label>NIM (*<small>Isikan NIM jika Mahasiswa</small>)</label>
        <?= $form->field($user, 'nim',['options' => ['tag' => false]])->input('nim', ['placeholder' => Yii::t('app', 'Enter ID'),'id'=>'nim'])->label(false) ?>
                
            </div>
    </div>
    </div>

    <div class="form-group">     
        <?= Html::submitButton($user->isNewRecord ? Yii::t('app', 'Create') 
            : Yii::t('app', 'Update'), ['class' => $user->isNewRecord 
            ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?= Html::a(Yii::t('app', 'Cancel'), ['user/index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
 
</div>

<?php 

$this->registerJs(' 


$(document).on(\'change\',\'#dosen\',function(e){
    e.preventDefault();
    $("#nim").val($(this).val());
    
});



', \yii\web\View::POS_READY);

?>
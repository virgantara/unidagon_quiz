<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

$this->title = Yii::t('app', 'Profil User') . ': ' . $model->username;
?>

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $user app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
      echo '<div class="flash alert alert-' . $key . '">' . $message . '<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">x</span></button></div>';
    }
    ?>

<div class="panel panel-profile">
    <div class="clearfix">
        <!-- LEFT COLUMN -->
        <div class="profile-left">
            <!-- PROFILE HEADER -->
            <div class="profile-header">
                <div class="overlay"></div>
                <div class="profile-main">
                    <i class="lnr lnr-user"></i>
                    <h3 class="name"><?=$dosen->nama_dosen;?></h3>
                    <span class="online-status status-available">Available</span>
                </div>
                <div class="profile-stat">
                    <div class="row">
                        <div class="col-md-4 stat-item">
                            0 <span>Projects</span>
                        </div>
                        <div class="col-md-4 stat-item">
                            0 <span>Awards</span>
                        </div>
                        <div class="col-md-4 stat-item">
                            0 <span>Points</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE HEADER -->
            <!-- PROFILE DETAIL -->
            <div class="profile-detail">
                <div class="profile-info">
                    <h4 class="heading">Basic Info</h4>
                    <ul class="list-unstyled list-justify">
                        <li>Place, Birthdate <span><?=$dosen->tempat_lahir_dosen;?>, <?=$dosen->tgl_lahir_dosen;?></span></li>
                        <li>Mobile <span><?=$dosen->no_hp_dosen;?></span></li>
                        <li>Email <span><?=$model->email;?></span></li>
                        <li>NIDN <span><?=$dosen->nidn_asli;?></span></li>
                    </ul>
                </div>
                
                <div class="profile-info">
                    <h4 class="heading">About</h4>
                    <p><!-- Interactively fashion excellent information after distinctive outsourcing. --></p>
                </div>
                <div class="text-center">
                	<a href="<?=Url::to(['simak-masterdosen/update-profil']);?>" class="btn btn-primary"><i class="fa fa-user"></i> Edit Profile</a>
                	<a href="<?=Url::to(['user/ubah-akun']);?>" class="btn btn-primary"><i class="fa fa-lock"></i> Ganti Password</a>
                </div>
            </div>
            <!-- END PROFILE DETAIL -->
        </div>
        <!-- END LEFT COLUMN -->
        <!-- RIGHT COLUMN -->
        <div class="profile-right">
            <h4 class="heading"><?=$dosen->nama_dosen;?>'s performances</h4>
            <!-- AWARDS -->
            <div class="awards">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="award-item">
                            <div class="hexagon">
                                <span class="lnr lnr-sun award-icon"></span>
                            </div>
                            <span>Most Bright Idea</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="award-item">
                            <div class="hexagon">
                                <span class="lnr lnr-clock award-icon"></span>
                            </div>
                            <span>Most On-Time</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="award-item">
                            <div class="hexagon">
                                <span class="lnr lnr-magic-wand award-icon"></span>
                            </div>
                            <span>Problem Solver</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="award-item">
                            <div class="hexagon">
                                <span class="lnr lnr-heart award-icon"></span>
                            </div>
                            <span>Most Loved</span>
                        </div>
                    </div>
                </div>
                <div class="text-center"><a href="#" class="btn btn-default">See all awards</a></div>
            </div>
            <!-- END AWARDS -->
            <!-- TABBED CONTENT -->
            <div class="custom-tabs-line tabs-line-bottom left-aligned">
                <ul class="nav" role="tablist">
                    <li class="active"><a href="#tab-bottom-left1" role="tab" data-toggle="tab">Recent Activity</a></li>
                    <li><a href="#tab-bottom-left2" role="tab" data-toggle="tab">Projects <span class="badge">7</span></a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tab-bottom-left1">
                    <ul class="list-unstyled activity-timeline">
                       <!--  <li>
                            <i class="fa fa-comment activity-icon"></i>
                            <p>Commented on post <a href="#">Prototyping</a> <span class="timestamp">2 minutes ago</span></p>
                        </li>
                        -->
                    </ul>
                    <div class="margin-top-30 text-center"><a href="#" class="btn btn-default">See all activity</a></div>
                </div>
                <div class="tab-pane fade" id="tab-bottom-left2">
                    <div class="table-responsive">
                        <table class="table project-table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Progress</th>
                                    <th>Leader</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                      <!--           <tr>
                                    <td><a href="#">Spot Media</a></td>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                                <span>60% Complete</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td><img src="assets/img/user2.png" alt="Avatar" class="avatar img-circle"> <a href="#">Michael</a></td>
                                    <td><span class="label label-success">ACTIVE</span></td>
                                </tr> -->
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END TABBED CONTENT -->
        </div>
        <!-- END RIGHT COLUMN -->
    </div>
</div>


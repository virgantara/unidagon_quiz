<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vertical-align-wrap">
    <div class="vertical-align-middle">
        <div class="auth-box ">
            <div class="left">
                <div class="content">
                    <div class="header">
                        <div class="logo text-center"><img src="<?=Yii::$app->view->theme->baseUrl;?>/assets/img/logounida.png" alt="Klorofil Logo"></div>
                        <p class="lead">Login to your account</p>
                    </div>
                    <?php $form = ActiveForm::begin(['class' => 'form-auth-small']); ?>
                    
                        <div class="form-group">
                            <label for="signin-email" class="control-label sr-only">Email</label>
                            <?= $form->field($model, 'username',['options' => ['tag' => false]])->textInput(
                ['placeholder' => Yii::t('app', 'Enter your username'), 'autofocus' => true,'class'=>'form-control'])->label(false) ?>
                        </div>
                        <div class="form-group">
                            <label for="signin-password" class="control-label sr-only">Password</label>
                            <?= $form->field($model, 'password',['options' => ['tag' => false]])->passwordInput(
                ['placeholder' => Yii::t('app', 'Enter your password'),'class'=>'form-control'])->label(false) ?>
                        </div>
                        
                        <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                        <div class="form-group clearfix">
                                <span>or</span>
                        </div>
                        <a href="<?=\yii\helpers\Url::to(['/site/auth','authclient'=>'google']);?>" class="btn btn-block btn-danger btn-lg">
          <i class="fa fa-google-plus mr-2"></i> Sign in using Google+
        </a>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="right">
                <div class="overlay"></div>
                <div class="content text">
                    <h1 class="heading">Sistem Informasi Akademik (SIAKAD) Universitas Darussalam Gontor</h1>
                    <p>by UNIDA ICT Team (PPTIK)</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

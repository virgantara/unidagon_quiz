<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

use app\assets\DashboardAsset;

DashboardAsset::register($this);


$alphabet = range('A', 'Z');
$tahun_id = \app\helpers\MyHelper::getTahunAktif();
$mod_tahun = $tahun_id % 2;
$list_semester = [
  1 => [1,3,5,7],
  0 => [2,4,6,8],
];

$this->title = Yii::t('app', Yii::$app->name);

?>
<h3 class="page-title">Selamat Datang di Sistem Informasi Akademik (SIAKAD) Universitas Darussalam Gontor</h3>

<?php
if(Yii::$app->user->can('Mahasiswa'))
{
  ?>
  <div class="panel panel-headline">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-info"></i> Info</h3>
      <p class="panel-subtitle"></p>
    </div>
    <div class="panel-body">
      <div class="row">
        
        <div class="col-md-12">
          <?php 
            $label = "";
            $txt = "";
            if($mhs->semester >= 7)
            {
              switch($mhs->is_eligible)
              {
                case 0:
                $label = 'danger';
                $txt = "Mohon maaf, Anda belum bisa mengikuti wisuda";
                break;
                case 1:
                $label = 'success';
                $txt = "Selamat, Anda sudah bisa mengikuti wisuda";
                break;  
              }
            }
            ?>

            <div class="alert alert-<?=$label;?>"><?=$txt;?></div>
        </div>
        
        
        
   
      </div>


    </div>
  </div>
<div class="panel panel-headline">
    <div class="panel-heading">
      <h3 class="panel-title">Overview</h3>
      <p class="panel-subtitle"></p>
    </div>
    <div class="panel-body">
      <div class="row">
        
        <div class="col-md-6">
          <div class="metric">
            <span class="icon"><i class="fa fa-user"></i></span>
            <p>
              <span class="number"><?=$mhs->nim_mhs;?></span>
              <span class="title">NIM</span>
            </p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="metric">
            <span class="icon"><i class="fa fa-user"></i></span>
            <p>
              <span class="number"><?=$mhs->nama_mahasiswa;?></span>
              <span class="title">Mahasiswa</span>
            </p>
          </div>
        </div>

        
   
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="metric">
            <span class="icon"><i class="fa fa-money"></i></span>
            <p>
              <span class="number">Rp <?=\app\helpers\MyHelper::formatRupiah($saldo,2);?></span>
              <span class="title">Saldo</span>
            </p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="metric">
            <span class="icon"><i class="fa fa-map"></i></span>
            <p>
              <span class="number"><?=$mhs->va_code;?></span>
              <span class="title">VA Code</span>
            </p>

           
          </div>
        </div>
      </div>
      <!-- <div class="row">
        <div class="col-md-9">
          <div id="headline-chart" class="ct-chart"></div>
        </div>
        <div class="col-md-3">
          <div class="weekly-summary text-right">
            <span class="number"></span> <span class="percentage"><i class="fa fa-caret-up text-success"></i> 12%</span>
            <span class="info-label">Saldo</span>
          </div>
          <div class="weekly-summary text-right">
            <span class="number"></span>
            <span class="info-label">VA Code</span>
          </div>
          <div class="weekly-summary text-right">
            <span class="number">$65,938</span> <span class="percentage"><i class="fa fa-caret-down text-danger"></i> 8%</span>
            <span class="info-label">Total Income</span>
          </div>
        </div>
      </div> -->
    </div>
  </div>
  <?php
}

else if(!Yii::$app->user->can('Mahasiswa'))
{
 ?>

<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading">
        <h1 class="panel-title">Rekap Jumlah Mahasiswa Aktif</h1>
        
      </div>
      <div class="panel-body">
        <div class="table-responsive">
         <table class="table table-bordered">
          <thead>
          <tr>
            <th rowspan="3">Prodi</th>
            <th colspan="10" class="text-center">Semester</th>
            <th rowspan="3" class="text-center">Aktif</th>
            <th rowspan="3"  class="text-center">Non<br>Aktif</th>
            <th rowspan="3"  class="text-center">Lulus</th>
            <th rowspan="3"  class="text-center">Cuti</th>
            <th rowspan="3"  class="text-center">Keluar</th>
            <th rowspan="3"  class="text-center">DO</th>
          </tr>
          <tr>

            <th class="text-center" colspan="2">1/2</th>
            <th class="text-center" colspan="2">3/4</th>
            <th class="text-center" colspan="2">5/6</th>
            <th class="text-center" colspan="2">7/8</th>
            <th class="text-center" colspan="2"> >= 9</th>
            
          </tr>
           <tr>

            <th class="text-center">Aktif</th>
            <th class="text-center">Non<br>Aktif</th>
            <th class="text-center">Aktif</th>
            <th class="text-center">Non<br>Aktif</th>
            <th class="text-center">Aktif</th>
            <th class="text-center">Non<br>Aktif</th>
            <th class="text-center">Aktif</th>
            <th class="text-center">Non<br>Aktif</th>
            <th class="text-center">Aktif</th>
            <th class="text-center">Non<br>Aktif</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $total_aktif = 0;$total_non_aktif = 0;
          foreach($fakultas as $q => $f)
          {

            ?>
            <tr>
            <td colspan="17"><?=$alphabet[$q];?>. <?=$f->nama_fakultas;?></td>
            
          </tr>
            <?php 
            foreach($f->simakMasterprogramstudis as $p)
            {



          ?>
            <tr>
            <td><span  style="margin-left:20px"><?=$p->nama_prodi;?></span></td>
            <?php 

            $subtotal_aktif = 0;
            $subtotal_non_aktif = 0;
              for($i=0;$i<4;$i++)
              {
                $count_aktif = \app\models\SimakMastermahasiswa::find()->where([
                  'kode_prodi' => $p->kode_prodi,
                  'semester' => $list_semester[$mod_tahun][$i],
                  'status_aktivitas' => 'A'
                ])->count();

                $count_non_aktif = \app\models\SimakMastermahasiswa::find()->where([
                  'kode_prodi' => $p->kode_prodi,
                  'semester' => $list_semester[$mod_tahun][$i],
                  'status_aktivitas' => 'N'
                ])->count();

                $subtotal_aktif += $count_aktif;
                $subtotal_non_aktif += $count_non_aktif;
                ?>
                <td class="text-center"><?=Html::a($count_aktif,['simak-mastermahasiswa/list','semester'=>$list_semester[$mod_tahun][$i], 'prodi'=>$p->kode_prodi,'status'=>'A'],['target'=>'_blank']);?></td>
                <td class="text-center"><?=Html::a($count_non_aktif,['simak-mastermahasiswa/list','semester'=>$list_semester[$mod_tahun][$i], 'prodi'=>$p->kode_prodi,'status'=>'N'],['target'=>'_blank']);?></td>
                <?php 
              }
              
              $count_dewa = \app\models\SimakMastermahasiswa::find()->where([
                  'kode_prodi' => $p->kode_prodi,
                  
                  'status_aktivitas' => 'A'
                ]);

              $count_dewa->andWhere(['>','semester',8]);
              $count_dewa = $count_dewa->count();   

              $count_dewa_non = \app\models\SimakMastermahasiswa::find()->where([
                  'kode_prodi' => $p->kode_prodi,
                  'status_aktivitas' => 'N'
                ]);
              $count_dewa_non->andWhere(['>','semester',8]);
              $count_dewa_non = $count_dewa_non->count();   

              $subtotal_aktif += $count_dewa;
                $subtotal_non_aktif += $count_dewa_non;                  

              $count_lulus = \app\models\SimakMastermahasiswa::find()->where([
                  'kode_prodi' => $p->kode_prodi,
                  'status_aktivitas' => 'L'
              ])->count();

              $count_cuti = \app\models\SimakMastermahasiswa::find()->where([
                  'kode_prodi' => $p->kode_prodi,
                  'status_aktivitas' => 'C'
              ])->count();

              $count_keluar = \app\models\SimakMastermahasiswa::find()->where([
                  'kode_prodi' => $p->kode_prodi,
                  'status_aktivitas' => 'K'
              ])->count();

              $count_do = \app\models\SimakMastermahasiswa::find()->where([
                  'kode_prodi' => $p->kode_prodi,
                  'status_aktivitas' => 'D'
              ])->count();
              
            ?>
           <td class="text-center"><?=Html::a($count_dewa,['simak-mastermahasiswa/list','semester'=>9, 'prodi'=>$p->kode_prodi,'status'=>'A'],['target'=>'_blank']);?></td>
            
            <td class="text-center"><?=Html::a($count_dewa_non,['simak-mastermahasiswa/list','semester'=>9, 'prodi'=>$p->kode_prodi,'status'=>'N'],['target'=>'_blank']);?></td>
            <td class="text-center"><?=Html::a($subtotal_aktif,['simak-mastermahasiswa/list','semester'=>'', 'prodi'=>$p->kode_prodi,'status'=>'A'],['target'=>'_blank']);?></td>
            <td class="text-center"><?=Html::a($subtotal_non_aktif,['simak-mastermahasiswa/list','semester'=>'', 'prodi'=>$p->kode_prodi,'status'=>'N'],['target'=>'_blank']);?></td>
            <td class="text-center"><?=Html::a($count_lulus,['simak-mastermahasiswa/list','semester'=>'', 'prodi'=>$p->kode_prodi,'status'=>'L'],['target'=>'_blank']);?></td>
            <td class="text-center"><?=Html::a($count_cuti,['simak-mastermahasiswa/list','semester'=>'', 'prodi'=>$p->kode_prodi,'status'=>'C'],['target'=>'_blank']);?></td>
            <td class="text-center"><?=Html::a($count_keluar,['simak-mastermahasiswa/list','semester'=>'', 'prodi'=>$p->kode_prodi,'status'=>'K'],['target'=>'_blank']);?></td>
            <td class="text-center"><?=Html::a($count_do,['simak-mastermahasiswa/list','semester'=>'', 'prodi'=>$p->kode_prodi,'status'=>'D'],['target'=>'_blank']);?></td>
          </tr>

          <?php 

              $total_aktif += $subtotal_aktif;
              $total_non_aktif += $subtotal_non_aktif;
            }
          }
          ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="11" class="text-right">Total</td>
            <td class="text-center"><?=$total_aktif;?></td>
            <td class="text-center"><?=$total_non_aktif;?></td>
          </tr>
        </tfoot>
        </table>
          </div>
      </div>
    </div>
  </div>
</div>


<?php
}
$this->registerJs(' 
    
  

    // var data, options;

    // data = {
    //   labels: [\'Mon\', \'Tue\', \'Wed\', \'Thu\', \'Fri\', \'Sat\', \'Sun\'],
    //   series: [
    //     [23, 29, 24, 40, 25, 24, 35],
    //     [14, 25, 18, 34, 29, 38, 44],
    //   ]
    // };

    // options = {
    //   height: 300,
    //   showArea: true,
    //   showLine: false,
    //   showPoint: false,
    //   fullWidth: true,
    //   axisX: {
    //     showGrid: false
    //   },
    //   lineSmooth: false,
    // };

    // new Chartist.Line(\'#headline-chart\', data, options);


    //  data = {
    //   labels: [\'Jan\', \'Feb\', \'Mar\', \'Apr\', \'May\', \'Jun\', \'Jul\', \'Aug\', \'Sep\', \'Oct\', \'Nov\', \'Dec\'],
    //   series: [
    //     [200, 380, 350, 320, 410, 450, 570, 400, 555, 620, 750, 900],
    //   ]
    // };

    // // line chart
    // options = {
    //   height: "300px",
    //   showPoint: true,
    //   axisX: {
    //     showGrid: false
    //   },
    //   lineSmooth: false,
    // };

    // new Chartist.Line(\'#demo-line-chart\', data, options);

    // // bar chart
    // options = {
    //   height: "300px",
    //   axisX: {
    //     showGrid: false
    //   },
    // };

    // new Chartist.Bar(\'#demo-bar-chart\', data, options);


    // // area chart
    // options = {
    //   height: "270px",
    //   showArea: true,
    //   showLine: false,
    //   showPoint: false,
    //   axisX: {
    //     showGrid: false
    //   },
    //   lineSmooth: false,
    // };

    // new Chartist.Line(\'#demo-area-chart\', data, options);


    // // multiple chart
    // var data = {
    //   labels: [\'Jan\', \'Feb\', \'Mar\', \'Apr\', \'May\', \'Jun\', \'Jul\', \'Aug\', \'Sep\', \'Oct\', \'Nov\', \'Dec\'],
    //   series: [{
    //     name: \'series-real\',
    //     data: [200, 380, 350, 320, 410, 450, 570, 400, 555, 620, 750, 900],
    //   }, {
    //     name: \'series-projection\',
    //     data: [240, 350, 360, 380, 400, 450, 480, 523, 555, 600, 700, 800],
    //   }]
    // };

    // var options = {
    //   fullWidth: true,
    //   lineSmooth: false,
    //   height: "270px",
    //   low: 0,
    //   high: \'auto\',
    //   series: {
    //     \'series-projection\': {
    //       showArea: true,
    //       showPoint: false,
    //       showLine: false
    //     },
    //   },
    //   axisX: {
    //     showGrid: false,

    //   },
    //   axisY: {
    //     showGrid: false,
    //     onlyInteger: true,
    //     offset: 0,
    //   },
    //   chartPadding: {
    //     left: 20,
    //     right: 20
    //   }
    // };

    // new Chartist.Line(\'#multiple-chart\', data, options);

', \yii\web\View::POS_READY);

?>

<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::$app->name;
?>
<style type="text/css">
  .btn-akun{
    background-color: #ffffff;
    color:#000;
    border-color: #000;
    border-style: solid !important
  }

  .btn-akun:hover{
    background-color: #000;
    color:#fff;
    border-color: #fff;
    border-style: solid !important
  }
</style>
<div class="container-contact100">
    <div class="wrap-contact100">
      <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options'=>[
          'class'=> 'contact100-form validate-form'
        ]

      ]); ?>
        <span class="contact100-form-title">
          Universitas Darussalam Gontor
        </span>


        <div class="wrap-input100 validate-input" data-validate="Name is required">
          <label class="label-input100" for="name">Username/NIM</label>
          <?= $form->field($model, 'username')->textInput(
                ['placeholder' => Yii::t('app', 'Enter your username'),'class'=>'input100', 'autofocus' => true])->label(false) ?>
          <span class="focus-input100"></span>
        </div>


        <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
          <label class="label-input100" for="email">Password</label>
         
          <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Enter your password'),'class'=>'input100'])->label(false) ?>
          <span class="focus-input100"></span>
        </div>

        

        <div class="container-contact100-form-btn">

        </div><!-- /.widget-main -->
              <div class="toolbar clearfix">
          
          <button type="submit" class="contact100-form-btn">
            Login
          </button>

        </div>

       <div class="container-contact100-form-btn">

          <a href="#" >
            Lupa NIM/Password?
          </a>
          
        </div>
        <div class="container-contact100-form-btn">

          <a href="<?=\yii\helpers\Url::to(['/site/auth','authclient'=>'google']);?>" class="contact100-form-btn btn-akun" >
            Masuk dengan Akun Google
          </a>
          
        </div>
        <div class="container-contact100-form-btn">

          <a href="https://pmb.unida.gontor.ac.id" target="_blank" class="contact100-form-btn btn-akun" >
            Informasi PMB
          </a>
          
        </div>
      <?php ActiveForm::end(); ?>

        
      <div class="contact100-more flex-col-c-m" style="background-image: url('<?=Yii::$app->view->theme->baseUrl;?>/v18/images/unida.jpg');">
      </div>
    </div>
  </div>
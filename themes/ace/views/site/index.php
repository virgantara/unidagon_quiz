<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
// use app\assets\HighchartAsset;
$this->title = Yii::t('app', Yii::$app->name);

// HighchartAsset::register($this);
// $listAsrama = \app\models\Asrama::find()->all();
?>
<style type="text/css">
.containerAsrama {
    height: 130px;
}

</style>
<div class="alert alert-block alert-success">
    <button type="button" class="close" data-dismiss="alert">
        <i class="ace-icon fa fa-times"></i>
    </button>

    <i class="ace-icon fa fa-check green"></i>

    Welcome to Imtihan Roqmi (IRQOM) Universitas Darussalam Gontor
    
</div>


<?php
$script = '



';
$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);


?>

<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = Yii::t('app', 'Registrasi Ujian IRQOM');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="logo text-center"><img src="<?=Yii::$app->view->theme->baseUrl;?>/images/logo_pmb.png" alt="Klorofil Logo" width="60%"></div>

<div class="user text-center">
	
	<h1 >Masukkan 6 digit Token Tes Anda</h1>
</div>
<?php $form = ActiveForm::begin([
	'options' => [
		'class' => 'form-horizontal'
	]
]); ?>   <?php
     foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
         echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
     } ?>
	<div class="input-group">
		<?= $form->field($model, 'token',['options' => ['tag' => false]])->textInput(['class'=>'form-control input-lg','placeholder'=>'Enter your token...','maxlength' => 6,'minlength'=>6])->label(false) ?>
		<span class="input-group-btn"><button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-arrow-right"></i> Ikut Tes Sekarang</button></span>
	</div>
<?php ActiveForm::end();?>
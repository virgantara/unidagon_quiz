<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\QuizAsset;
use app\widgets\Alert;
use yii\widgets\Menu;
use yii\helpers\Html;

QuizAsset::register($this);

$theme = $this->theme;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="fullscreen-bg">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<?php $this->head(); ?>
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="icon" type="image/ico" sizes="16x16" href="<?=Yii::$app->view->theme->baseUrl;?>/images/favicon.ico">
	<?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    
</head>

<body>
	<?php $this->beginBody() ?>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				
				<div class="auth-box lockscreen clearfix">
					<div class="content">
						<?=$content;?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
	<?php $this->endBody() ?>
</body>

</html>

<?php $this->endPage() ?>

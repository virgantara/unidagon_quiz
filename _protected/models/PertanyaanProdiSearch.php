<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PertanyaanProdi;

/**
 * PertanyaanProdiSearch represents the model behind the search form of `app\models\PertanyaanProdi`.
 */
class PertanyaanProdiSearch extends PertanyaanProdi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'prodi_id'], 'integer'],
            [['jenis_pertanyaan_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PertanyaanProdi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'prodi_id' => $this->prodi_id,
        ]);

        $query->andFilterWhere(['like', 'jenis_pertanyaan_id', $this->jenis_pertanyaan_id]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_pertanyaan_prodi".
 *
 * @property int $id
 * @property int $prodi_id
 * @property string $jenis_pertanyaan_id
 *
 * @property JenisPertanyaan $jenisPertanyaan
 * @property MProdi $prodi
 */
class PertanyaanProdi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_pertanyaan_prodi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prodi_id', 'jenis_pertanyaan_id'], 'required'],
            [['prodi_id'], 'integer'],
            [['jenis_pertanyaan_id'], 'string', 'max' => 5],
            [['prodi_id'], 'exist', 'skipOnError' => true, 'targetClass' => MProdi::className(), 'targetAttribute' => ['prodi_id' => 'id_prodi']],
            [['jenis_pertanyaan_id'], 'exist', 'skipOnError' => true, 'targetClass' => JenisPertanyaan::className(), 'targetAttribute' => ['jenis_pertanyaan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prodi_id' => 'Prodi ID',
            'jenis_pertanyaan_id' => 'Jenis Pertanyaan ID',
        ];
    }

    /**
     * Gets query for [[JenisPertanyaan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenisPertanyaan()
    {
        return $this->hasOne(JenisPertanyaan::className(), ['id' => 'jenis_pertanyaan_id']);
    }

    /**
     * Gets query for [[Prodi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdi()
    {
        return $this->hasOne(MProdi::className(), ['id_prodi' => 'prodi_id']);
    }
}

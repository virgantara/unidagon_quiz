<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_peserta".
 *
 * @property int $id
 * @property string $nama
 * @property string $tempat_lahir
 * @property string|null $tanggal_lahir
 * @property string $jk
 * @property int $agama_id
 * @property string $status_warga_negara
 * @property string $nik
 * @property string|null $nisn
 * @property string|null $nama_ibu_kandung
 * @property string|null $jalan
 * @property string|null $dusun
 * @property string|null $kelurahan
 * @property int $kecamatan_id
 * @property string|null $rt
 * @property string|null $rw
 * @property string|null $kodepos
 * @property int|null $jenis_tinggal_id
 * @property int|null $alat_transportasi_id
 * @property string|null $telepon
 * @property string $email
 * @property string|null $hp
 * @property int|null $penerima_kps
 * @property string|null $no_kps
 * @property int|null $is_pesantren
 * @property string|null $nama_pesantren
 * @property int|null $tahun_lulus
 * @property int|null $lama_pendidikan
 * @property string|null $takhassus
 * @property string|null $sd
 * @property string|null $smp
 * @property string|null $sma
 * @property string|null $nama_ayah
 * @property string|null $nik_ayah
 * @property string|null $tanggal_lahir_ayah
 * @property int|null $pendidikan_ayah_id
 * @property int|null $pekerjaan_ayah_id
 * @property int|null $penghasilan_ayah_id
 * @property string|null $nama_ibu
 * @property string|null $nik_ibu
 * @property string|null $tanggal_lahir_ibu
 * @property int|null $pendidikan_ibu_id
 * @property int|null $pekerjaan_ibu_id
 * @property int|null $penghasilan_ibu_id
 * @property string|null $nama_wali
 * @property string|null $tanggal_lahir_wali
 * @property int|null $pendidikan_wali_id
 * @property int|null $pekerjaan_wali_id
 * @property int|null $penghasilan_wali_id
 * @property int|null $prodi_tujuan1
 * @property int|null $prodi_tujuan2
 * @property int|null $prodi_tujuan3
 * @property int|null $is_kmi
 * @property int|null $lama_rencana_kuliah
 * @property int|null $kampus_id
 * @property int|null $is_active
 * @property int|null $is_accepted
 * @property string|null $foto_path
 * @property string|null $token
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Agama $agama
 * @property Propinsi $alatTransportasi
 * @property Jawaban[] $jawabans
 * @property JenisTinggal $jenisTinggal
 * @property Kecamatan $kecamatan
 * @property Pekerjaan $pekerjaanAyah
 * @property Pekerjaan $pekerjaanIbu
 * @property Pekerjaan $pekerjaanWali
 * @property Pendidikan $pendidikanAyah
 * @property Pendidikan $pendidikanIbu
 * @property Pendidikan $pendidikanWali
 * @property Penghasilan $penghasilanAyah
 * @property Penghasilan $penghasilanIbu
 * @property Penghasilan $penghasilanWali
 * @property PesertaSyarat[] $pesertaSyarats
 * @property MProdi $prodiTujuan1
 * @property MProdi $prodiTujuan2
 * @property MProdi $prodiTujuan3
 */
class Peserta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_peserta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'tempat_lahir', 'jk', 'agama_id', 'status_warga_negara', 'nik', 'kecamatan_id', 'email'], 'required'],
            [['tanggal_lahir', 'tanggal_lahir_ayah', 'tanggal_lahir_ibu', 'tanggal_lahir_wali', 'created_at', 'updated_at'], 'safe'],
            [['agama_id', 'kecamatan_id', 'jenis_tinggal_id', 'alat_transportasi_id', 'penerima_kps', 'is_pesantren', 'tahun_lulus', 'lama_pendidikan', 'pendidikan_ayah_id', 'pekerjaan_ayah_id', 'penghasilan_ayah_id', 'pendidikan_ibu_id', 'pekerjaan_ibu_id', 'penghasilan_ibu_id', 'pendidikan_wali_id', 'pekerjaan_wali_id', 'penghasilan_wali_id', 'prodi_tujuan1', 'prodi_tujuan2', 'prodi_tujuan3', 'is_kmi', 'lama_rencana_kuliah', 'kampus_id', 'is_active', 'is_accepted'], 'integer'],
            [['nama', 'tempat_lahir', 'nama_ibu_kandung', 'jalan', 'dusun', 'kelurahan', 'telepon', 'email', 'hp', 'no_kps', 'nama_pesantren', 'takhassus', 'sd', 'smp', 'sma', 'nama_ayah', 'nama_ibu', 'nama_wali', 'foto_path', 'token'], 'string', 'max' => 255],
            [['jk', 'status_warga_negara', 'rt', 'rw'], 'string', 'max' => 5],
            [['nik'], 'string', 'max' => 30],
            [['nisn'], 'string', 'max' => 50],
            [['kodepos'], 'string', 'max' => 10],
            [['nik_ayah', 'nik_ibu'], 'string', 'max' => 40],
            
            [['prodi_tujuan1'], 'exist', 'skipOnError' => true, 'targetClass' => MProdi::className(), 'targetAttribute' => ['prodi_tujuan1' => 'kode_prodi']],
            [['prodi_tujuan2'], 'exist', 'skipOnError' => true, 'targetClass' => MProdi::className(), 'targetAttribute' => ['prodi_tujuan2' => 'kode_prodi']],
            [['prodi_tujuan3'], 'exist', 'skipOnError' => true, 'targetClass' => MProdi::className(), 'targetAttribute' => ['prodi_tujuan3' => 'kode_prodi']],
          
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jk' => 'Jk',
            'agama_id' => 'Agama ID',
            'status_warga_negara' => 'Status Warga Negara',
            'nik' => 'Nik',
            'nisn' => 'Nisn',
            'nama_ibu_kandung' => 'Nama Ibu Kandung',
            'jalan' => 'Jalan',
            'dusun' => 'Dusun',
            'kelurahan' => 'Kelurahan',
            'kecamatan_id' => 'Kecamatan ID',
            'rt' => 'Rt',
            'rw' => 'Rw',
            'kodepos' => 'Kodepos',
            'jenis_tinggal_id' => 'Jenis Tinggal ID',
            'alat_transportasi_id' => 'Alat Transportasi ID',
            'telepon' => 'Telepon',
            'email' => 'Email',
            'hp' => 'Hp',
            'penerima_kps' => 'Penerima Kps',
            'no_kps' => 'No Kps',
            'is_pesantren' => 'Is Pesantren',
            'nama_pesantren' => 'Nama Pesantren',
            'tahun_lulus' => 'Tahun Lulus',
            'lama_pendidikan' => 'Lama Pendidikan',
            'takhassus' => 'Takhassus',
            'sd' => 'Sd',
            'smp' => 'Smp',
            'sma' => 'Sma',
            'nama_ayah' => 'Nama Ayah',
            'nik_ayah' => 'Nik Ayah',
            'tanggal_lahir_ayah' => 'Tanggal Lahir Ayah',
            'pendidikan_ayah_id' => 'Pendidikan Ayah ID',
            'pekerjaan_ayah_id' => 'Pekerjaan Ayah ID',
            'penghasilan_ayah_id' => 'Penghasilan Ayah ID',
            'nama_ibu' => 'Nama Ibu',
            'nik_ibu' => 'Nik Ibu',
            'tanggal_lahir_ibu' => 'Tanggal Lahir Ibu',
            'pendidikan_ibu_id' => 'Pendidikan Ibu ID',
            'pekerjaan_ibu_id' => 'Pekerjaan Ibu ID',
            'penghasilan_ibu_id' => 'Penghasilan Ibu ID',
            'nama_wali' => 'Nama Wali',
            'tanggal_lahir_wali' => 'Tanggal Lahir Wali',
            'pendidikan_wali_id' => 'Pendidikan Wali ID',
            'pekerjaan_wali_id' => 'Pekerjaan Wali ID',
            'penghasilan_wali_id' => 'Penghasilan Wali ID',
            'prodi_tujuan1' => 'Prodi Tujuan1',
            'prodi_tujuan2' => 'Prodi Tujuan2',
            'prodi_tujuan3' => 'Prodi Tujuan3',
            'is_kmi' => 'Is Kmi',
            'lama_rencana_kuliah' => 'Lama Rencana Kuliah',
            'kampus_id' => 'Kampus ID',
            'is_active' => 'Is Active',
            'is_accepted' => 'Is Accepted',
            'foto_path' => 'Foto Path',
            'token' => 'Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

  
    /**
     * Gets query for [[ProdiTujuan1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdiTujuan1()
    {
        return $this->hasOne(MProdi::className(), ['kode_prodi' => 'prodi_tujuan1']);
    }

    /**
     * Gets query for [[ProdiTujuan2]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdiTujuan2()
    {
        return $this->hasOne(MProdi::className(), ['kode_prodi' => 'prodi_tujuan2']);
    }

    /**
     * Gets query for [[ProdiTujuan3]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProdiTujuan3()
    {
        return $this->hasOne(MProdi::className(), ['kode_prodi' => 'prodi_tujuan3']);
    }
}

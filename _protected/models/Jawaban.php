<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_jawaban".
 *
 * @property int $id
 * @property int $peserta_id
 * @property int $pertanyaan_id
 * @property string $jawaban
 * @property float|null $poin
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Pertanyaan $pertanyaan
 * @property Peserta $peserta
 */
class Jawaban extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_jawaban';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['peserta_id', 'pertanyaan_id'], 'required'],
            [['peserta_id', 'pertanyaan_id'], 'integer'],
            [['poin'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            
            [['pertanyaan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pertanyaan::className(), 'targetAttribute' => ['pertanyaan_id' => 'id']],
            [['peserta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Peserta::className(), 'targetAttribute' => ['peserta_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'peserta_id' => 'Peserta ID',
            'pertanyaan_id' => 'Pertanyaan ID',
            'jawaban' => 'Jawaban',
            'poin' => 'Poin',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Pertanyaan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertanyaan()
    {
        return $this->hasOne(Pertanyaan::className(), ['id' => 'pertanyaan_id']);
    }

    /**
     * Gets query for [[Peserta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeserta()
    {
        return $this->hasOne(Peserta::className(), ['id' => 'peserta_id']);
    }
}

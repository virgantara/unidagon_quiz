<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_subbagian".
 *
 * @property int $id
 * @property string $jenis_id
 * @property string $nama
 *
 * @property JenisPertanyaan $jenis
 * @property Pertanyaan[] $pertanyaans
 */
class Subbagian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_subbagian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_id', 'nama'], 'required'],
            [['jenis_id'], 'string', 'max' => 5],
            [['nama'], 'string', 'max' => 255],
            [['jenis_id'], 'exist', 'skipOnError' => true, 'targetClass' => JenisPertanyaan::className(), 'targetAttribute' => ['jenis_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_id' => 'Jenis ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * Gets query for [[Jenis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJenis()
    {
        return $this->hasOne(JenisPertanyaan::className(), ['id' => 'jenis_id']);
    }

    /**
     * Gets query for [[Pertanyaans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertanyaans()
    {
        return $this->hasMany(Pertanyaan::className(), ['subbagian_id' => 'id'])->orderBy(['nomor_soal' => SORT_ASC]);
    }
}

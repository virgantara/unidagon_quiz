<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Peserta;

/**
 * PesertaSearch represents the model behind the search form of `app\models\Peserta`.
 */
class PesertaSearch extends Peserta
{


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'agama_id', 'kecamatan_id', 'jenis_tinggal_id', 'alat_transportasi_id', 'penerima_kps', 'is_pesantren', 'tahun_lulus', 'lama_pendidikan', 'pendidikan_ayah_id', 'pekerjaan_ayah_id', 'penghasilan_ayah_id', 'pendidikan_ibu_id', 'pekerjaan_ibu_id', 'penghasilan_ibu_id', 'pendidikan_wali_id', 'pekerjaan_wali_id', 'penghasilan_wali_id', 'prodi_tujuan1', 'prodi_tujuan2', 'prodi_tujuan3', 'is_kmi', 'lama_rencana_kuliah', 'kampus_id'], 'integer'],
            [['nama', 'tempat_lahir', 'tanggal_lahir', 'jk', 'status_warga_negara', 'nik', 'nisn', 'nama_ibu_kandung', 'jalan', 'dusun', 'kelurahan', 'rt', 'rw', 'kodepos', 'telepon', 'email', 'hp', 'no_kps', 'nama_pesantren', 'takhassus', 'sd', 'smp', 'sma', 'nama_ayah', 'nik_ayah', 'tanggal_lahir_ayah', 'nama_ibu', 'nik_ibu', 'tanggal_lahir_ibu', 'nama_wali', 'tanggal_lahir_wali', 'created_at', 'updated_at','statusTerima','namaProdi1','namaProdi2','namaProdi3','token'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Peserta::find();
       
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);

        

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
     

     

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['like', 'jk', $this->jk])
            ->andFilterWhere(['like', 'status_warga_negara', $this->status_warga_negara])
            ->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'nisn', $this->nisn])
            ->andFilterWhere(['like', 'nama_ibu_kandung', $this->nama_ibu_kandung])
            ->andFilterWhere(['like', 'jalan', $this->jalan])
            ->andFilterWhere(['like', 'dusun', $this->dusun])
            ->andFilterWhere(['like', 'kelurahan', $this->kelurahan])
            ->andFilterWhere(['like', 'rt', $this->rt])
            ->andFilterWhere(['like', 'rw', $this->rw])
            ->andFilterWhere(['like', 'kodepos', $this->kodepos])
            ->andFilterWhere(['like', 'telepon', $this->telepon])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'hp', $this->hp])
            ->andFilterWhere(['like', 'no_kps', $this->no_kps])
            ->andFilterWhere(['like', 'nama_pesantren', $this->nama_pesantren])
            ->andFilterWhere(['like', 'takhassus', $this->takhassus])
            ->andFilterWhere(['like', 'sd', $this->sd])
            ->andFilterWhere(['like', 'smp', $this->smp])
            ->andFilterWhere(['like', 'sma', $this->sma])
            ->andFilterWhere(['like', 'nama_ayah', $this->nama_ayah])
            ->andFilterWhere(['like', 'nik_ayah', $this->nik_ayah])
            ->andFilterWhere(['like', 'nama_ibu', $this->nama_ibu])
            ->andFilterWhere(['like', 'nik_ibu', $this->nik_ibu])
            ->andFilterWhere(['like', 'nama_wali', $this->nama_wali]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_prodi".
 *
 * @property int $id_prodi
 * @property string|null $nama_prodi
 * @property int $urutan
 * @property int|null $kode_prodi
 *
 * @property PertanyaanProdi[] $pertanyaanProdis
 * @property Peserta[] $pesertas
 * @property Peserta[] $pesertas0
 * @property Peserta[] $pesertas1
 */
class MProdi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_prodi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urutan', 'kode_prodi'], 'integer'],
            [['nama_prodi'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_prodi' => 'Id Prodi',
            'nama_prodi' => 'Nama Prodi',
            'urutan' => 'Urutan',
            'kode_prodi' => 'Kode Prodi',
        ];
    }

    /**
     * Gets query for [[PertanyaanProdis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertanyaanProdis()
    {
        return $this->hasMany(PertanyaanProdi::className(), ['prodi_id' => 'id_prodi']);
    }

    /**
     * Gets query for [[Pesertas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas()
    {
        return $this->hasMany(Peserta::className(), ['prodi_tujuan1' => 'kode_prodi']);
    }

    /**
     * Gets query for [[Pesertas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas0()
    {
        return $this->hasMany(Peserta::className(), ['prodi_tujuan2' => 'kode_prodi']);
    }

    /**
     * Gets query for [[Pesertas1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPesertas1()
    {
        return $this->hasMany(Peserta::className(), ['prodi_tujuan3' => 'kode_prodi']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_pertanyaan".
 *
 * @property int $id
 * @property string $jenis_pertanyaan_id
 * @property string $nama
 * @property string $jawaban1
 * @property string $jawaban2
 * @property string $jawaban3
 * @property string $jawaban4
 * @property int $jawaban_benar
 * @property float|null $poin
 * @property string|null $foto_path
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Jawaban[] $jawabans
 * @property JenisPertanyaan $jenisPertanyaan
 */
class Pertanyaan extends \yii\db\ActiveRecord
{

    public $buktiFile;
    public $deleteFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_pertanyaan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'jawaban1', 'jawaban2', 'jawaban3', 'jawaban4', 'jawaban_benar','subbagian_id','nomor_soal','quiz_id'], 'required'],
            [['jawaban_benar'], 'integer'],
            [['poin'], 'number'],
            [['created_at', 'updated_at','deleteFile','quiz_id'], 'safe'],
            [['buktiFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg,bmp','maxSize'=>512000,'tooBig' => 'Batas file maksimal 512KB'],
            [['jawaban1', 'jawaban2', 'jawaban3', 'jawaban4', 'foto_path'], 'string', 'max' => 255],
            [['subbagian_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subbagian::className(), 'targetAttribute' => ['subbagian_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subbagian_id' => 'Subbagian',
            'nama' => 'Nama',
            'nomor_soal' => 'Nomor Soal',
            'jawaban1' => 'Jawaban 1',
            'jawaban2' => 'Jawaban 2',
            'jawaban3' => 'Jawaban 3',
            'jawaban4' => 'Jawaban 4',
            'jawaban_benar' => 'Jawaban Benar',
            'poin' => 'Poin',
            'foto_path' => 'Foto Path',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Jawabans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJawabans()
    {
        return $this->hasMany(Jawaban::className(), ['pertanyaan_id' => 'id']);
    }


    public function getSubbagian()
    {
        return $this->hasOne(Subbagian::className(), ['id' => 'subbagian_id']);
    }
}

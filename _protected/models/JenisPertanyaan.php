<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_jenis_pertanyaan".
 *
 * @property string $id
 * @property string $nama
 *
 * @property PertanyaanProdi[] $pertanyaanProdis
 * @property Subbagian[] $subbagians
 */
class JenisPertanyaan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_jenis_pertanyaan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nama'], 'required'],
            [['id'], 'string', 'max' => 5],
            [['nama'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * Gets query for [[PertanyaanProdis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPertanyaanProdis()
    {
        return $this->hasMany(PertanyaanProdi::className(), ['jenis_pertanyaan_id' => 'id']);
    }

    /**
     * Gets query for [[Subbagians]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubbagians()
    {
        return $this->hasMany(Subbagian::className(), ['jenis_id' => 'id']);
    }
}

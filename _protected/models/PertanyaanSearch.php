<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pertanyaan;

/**
 * PertanyaanSearch represents the model behind the search form of `app\models\Pertanyaan`.
 */
class PertanyaanSearch extends Pertanyaan
{

    public $namaSub;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'jawaban_benar'], 'integer'],
            [['subbagian_id', 'nama', 'jawaban1', 'jawaban2', 'jawaban3', 'jawaban4', 'foto_path', 'created_at', 'updated_at','namaSub','nomor_soal'], 'safe'],
            [['poin'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pertanyaan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!empty($this->subbagian_id))
        {
            $query->andWhere(['subbagian_id'=>$this->subbagian_id]);
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'jawaban_benar' => $this->jawaban_benar,
        //     'poin' => $this->poin,
        //     'created_at' => $this->created_at,
        //     'updated_at' => $this->updated_at,
        // ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jawaban1', $this->jawaban1])
            ->andFilterWhere(['like', 'jawaban2', $this->jawaban2])
            ->andFilterWhere(['like', 'jawaban3', $this->jawaban3])
            ->andFilterWhere(['like', 'jawaban4', $this->jawaban4])
            ->andFilterWhere(['like', 'foto_path', $this->foto_path]);

        return $dataProvider;
    }

    public static function getSubbagianList()
    {
        $list = [];

        foreach (Subbagian::find()->all() as $item_name) {
            $list[$item_name->id] = $item_name->nama;
        }

        return $list;
    }
}

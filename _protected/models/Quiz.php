<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "erp_quiz".
 *
 * @property int $id
 * @property string $nama
 * @property string $tanggal
 * @property string $time_start
 * @property string $time_end
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Quiz extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'erp_quiz';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'tanggal', 'time_start', 'time_end'], 'required'],
            [['tanggal', 'time_start', 'time_end', 'created_at', 'updated_at','buka'], 'safe'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'tanggal' => 'Tanggal',
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}

<?php

namespace app\controllers;

use Yii;
use app\models\Quiz;
use app\models\Prodi;
use app\models\Peserta;
use app\models\Subbagian;
use app\models\PesertaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\MyHelper;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\filters\AccessControl;
use tecnickcom\tcpdf\TCPDF;

/**
 * PesertaController implements the CRUD actions for Peserta model.
 */
class PesertaController extends Controller
{

    public $listKampus = [];
    public $listProdi = [];
    /**
     * {@inheritdoc}
     */

    public function __construct($id, $module, $config = [])
    {

        // $this->listProdi = ArrayHelper::map(Prodi::find()->all(),'kode_prodi','nama_prodi');
        return parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','update','print','view'],
                // 'denyCallback' => function ($rule, $action) {

                //     throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                // },
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','approval','hasil','export-hasil'],
                        'roles' => ['operatorCabang','admin'],
                    ],

                    [
                        'allow' => true,
                        'actions' => ['create','update','print','view','syarat'],
                        'roles' => ['member'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionExportHasil()
    {
        $quiz = Quiz::find()->where(['buka' => 'Y'])->one();
        $this->layout = 'none';
        $pesertas = (new \yii\db\Query())
            ->select(['p.nama', 'p.id','p1.nama_prodi as p1','p2.nama_prodi as p2','p3.nama_prodi as p3'])
            ->from('erp_peserta p')
            ->join('JOIN','erp_jawaban j','p.id = j.peserta_id')
            ->join('JOIN','erp_pertanyaan pp','pp.id = j.pertanyaan_id')
            ->join('LEFT JOIN','m_prodi p1','p1.kode_prodi = p.prodi_tujuan1')
            ->join('LEFT JOIN','m_prodi p2','p2.kode_prodi = p.prodi_tujuan2')
            ->join('LEFT JOIN','m_prodi p3','p3.kode_prodi = p.prodi_tujuan3')
            ->andWhere(['pp.quiz_id' => $quiz->id])
            ->groupBy(['p.nama','p.id','p1.nama_prodi','p2.nama_prodi','p3.nama_prodi'])
            ->all();
            
        $subbagian = Subbagian::find()->all();
        $results = [];
        foreach($pesertas as $m)
        {

            foreach($subbagian as $sub)
            {
                $sum = (new \yii\db\Query())
                    ->select(['j.poin'])
                    ->from('erp_peserta p')
                    ->join('JOIN','erp_jawaban j','p.id = j.peserta_id')
                    ->join('JOIN','erp_pertanyaan pp','pp.id = j.pertanyaan_id')
                    ->where(['pp.subbagian_id' => $sub->id])
                    ->andWhere(['j.peserta_id' => $m['id']])
                    ->andWhere(['pp.quiz_id' => $quiz->id])
                    ->sum('j.poin');

                $results[$m['id']][$sub->id] = $sum;
            }

        }

        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachment; filename=hasil.xls");  //File name extension was wrong
            header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");;
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);


        echo $this->renderPartial('export_hasil', [
            'pesertas' => $pesertas,
            'subbagian' => $subbagian,
            'results' => $results,
            'export' => $export
        ]);

        die();
    }

    public function actionHasil()
    {
        $quiz = Quiz::find()->where(['buka' => 'Y'])->one();
        $pesertas = (new \yii\db\Query())
            ->select(['p.nama', 'p.id','p1.nama_prodi as p1','p2.nama_prodi as p2','p3.nama_prodi as p3'])
            ->from('erp_peserta p')
            ->join('JOIN','erp_jawaban j','p.id = j.peserta_id')
            ->join('JOIN','erp_pertanyaan pp','pp.id = j.pertanyaan_id')
            ->join('LEFT JOIN','m_prodi p1','p1.kode_prodi = p.prodi_tujuan1')
            ->join('LEFT JOIN','m_prodi p2','p2.kode_prodi = p.prodi_tujuan2')
            ->join('LEFT JOIN','m_prodi p3','p3.kode_prodi = p.prodi_tujuan3')
            ->andWhere(['pp.quiz_id' => $quiz->id])
            ->groupBy(['p.nama','p.id','p1.nama_prodi','p2.nama_prodi','p3.nama_prodi'])
            ->all();
            
        $subbagian = Subbagian::find()->all();
        $results = [];
        foreach($pesertas as $m)
        {

            foreach($subbagian as $sub)
            {
                $sum = (new \yii\db\Query())
                    ->select(['j.poin'])
                    ->from('erp_peserta p')
                    ->join('JOIN','erp_jawaban j','p.id = j.peserta_id')
                    ->join('JOIN','erp_pertanyaan pp','pp.id = j.pertanyaan_id')
                    ->where(['pp.subbagian_id' => $sub->id])
                    ->andWhere(['j.peserta_id' => $m['id']])
                    ->andWhere(['pp.quiz_id' => $quiz->id])
                    ->sum('j.poin');

                $results[$m['id']][$sub->id] = $sum;
            }

        }



        return $this->render('hasil', [
            'pesertas' => $pesertas,
            'subbagian' => $subbagian,
            'results' => $results,
            'export' => $export
        ]);
    }

    /**
     * Lists all Peserta models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new PesertaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listProdi' => $this->listProdi,
            'listKampus' => $this->listKampus
        ]);
    }

    /**
     * Displays a single Peserta model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token'=>$client_token];
        $response = $client->get('/p/list', [],$headers)->send();
        
        // $listProdi = [];
        
        if ($response->isOk) {
            $tmp = $response->data['values'];

            foreach($tmp as $item){
                $this->listProdi[$item['kode_prodi']] = $item['nama_prodi'];
            }
        }

        $response = $client->get('/k/list', [],$headers)->send();
        
        // $listKampus = [];
        
        if ($response->isOk) {
            $tmp = $response->data['values'];

            foreach($tmp as $item){
                $this->listKampus[$item['kode_kampus']] = $item['nama_kampus'];
            }
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'listProdi' => $this->listProdi,
            'listKampus' => $this->listKampus
        ]);
    }

    public function actionPrint($id)
    {

        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token'=>$client_token];
        $response = $client->get('/p/list', [],$headers)->send();
        
        // $listProdi = [];
        
        if ($response->isOk) {
            $tmp = $response->data['values'];

            foreach($tmp as $item){
                $this->listProdi[$item['kode_prodi']] = $item['nama_prodi'];
            }
        }

        $response = $client->get('/k/list', [],$headers)->send();
        
        // $listKampus = [];
        
        if ($response->isOk) {
            $tmp = $response->data['values'];

            foreach($tmp as $item){
                $this->listKampus[$item['kode_kampus']] = $item['nama_kampus'];
            }
        }

        try
        {

         
            ob_start();
            

            $this->layout = '';
            ob_start();
            echo $this->renderPartial('print', [
                'model' => $this->findModel($id),
                'listProdi' => $this->listProdi,
                'listKampus' => $this->listKampus
            ]);
            $data = ob_get_clean();
            ob_start();
            $pdf = new \TCPDF();  
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->SetFont('dejavusans', '', 8);
            $pdf->AddPage();
            $pdf->writeHTML($data);

            $pdf->Output();
        }
        catch(\HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
        die();
    }

    public function actionProfil()
    {
        $model = Peserta::find()->where(['email'=>Yii::$app->user->identity->email])->one();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionApproval()
    {

        if(Yii::$app->request->isPost)
        {

            $dataku = $_POST['dataku'];
            $id = $dataku['id'];
            $model = $this->findModel($id);
            if(Yii::$app->user->can('admin'))
            {
                $model->is_accepted = $dataku['kode'];
                $model->save(false,['is_accepted']);

                if($model->is_accepted == '1')
                {
                    $emailTemplate = $this->renderPartial('emailTerima',['model'=>$model]);
                    Yii::$app->mailer->compose()
                    ->setTo($model->email)
                    ->setFrom([Yii::$app->params['supportEmail'] => 'PMB UNIDA Gontor'])
                    ->setSubject('Pengumuman Penerimaan Mahasiswa Baru Universitas Darussalam Gontor')
                    ->setHtmlBody($emailTemplate)
                    ->send();
                }
            }

            $results = [
                'code' => 200,
                'msg' => "Approval Berhasil"
            ];
            echo json_encode($results);        
        }

        die();
    }


    /**
     * Creates a new Peserta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Peserta();
        $model->email = Yii::$app->user->identity->email;
        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token'=>$client_token];
        $response = $client->get('/p/list', [],$headers)->send();
        
        $listProdi = [];
        
        if ($response->isOk) {
            $tmp = $response->data['values'];

            foreach($tmp as $item){
                $listProdi[$item['kode_prodi']] = $item['nama_prodi'];
            }
        }

        $response = $client->get('/k/list', [],$headers)->send();
        
        $listKampus = [];
        
        if ($response->isOk) {
            $tmp = $response->data['values'];

            foreach($tmp as $item){
                $listKampus[$item['kode_kampus']] = $item['nama_kampus'];
            }
        }

        

        if ($model->load(Yii::$app->request->post())) {
            $arr = [$model->prodi_tujuan1];

            if(in_array($model->prodi_tujuan2, $arr) || in_array($model->prodi_tujuan3, $arr)){
                // echo 'a' ;exit;
                $model->addError($attribute, 'Silakan pilih prodi yang berbeda-beda.');
            }

            else if($model->validate())
            {
                $model->save();
                $emailTemplate = $this->renderPartial('email',['model'=>$model]);
                Yii::$app->mailer->compose()
                ->setTo($model->email)
                ->setFrom([Yii::$app->params['supportEmail'] => 'PMB UNIDA Gontor'])
                ->setSubject('Registrasi UNIDA Gontor')
                ->setHtmlBody($emailTemplate)
                ->send();
                Yii::$app->session->setFlash('success', "Data tersimpan");
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
        }

        return $this->render('create', [
            'model' => $model,
            'listProdi' => $listProdi,
            'listKampus' => $listKampus
        ]);
    }

    /**
     * Updates an existing Peserta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

     

        
        if ($model->load(Yii::$app->request->post())) {
            $model->save(false,['token']);
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['update', 'id' => $model->id]);
           
        }

        return $this->render('update', [
            'model' => $model,
            
        ]);
    }

    /**
     * Deletes an existing Peserta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Peserta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Peserta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Peserta::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace app\controllers;

use Yii;

use app\models\Quiz;
use app\models\Jawaban;
use app\models\Subbagian;
use app\models\Peserta;
use app\models\PertanyaanProdi;
use app\models\JenisPertanyaan;
use app\models\Pertanyaan;
use app\models\PertanyaanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\filters\AccessControl;

use yii\httpclient\Client;
/**
 * PertanyaanController implements the CRUD actions for Pertanyaan model.
 */
class PertanyaanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','update','print','view','delete'],
                // 'denyCallback' => function ($rule, $action) {

                //     throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                // },
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','create','update','print','view','delete'],
                        'roles' => ['operatorCabang','admin'],
                    ],

                    
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionHasil()
    {
        $session = Yii::$app->session;
        if(!$session->has('token'))
        {
            $this->refresh();
            Yii::$app->session->setFlash('warning', "Maaf, sesi Anda telah habis.");
            return $this->redirect(['site/test']);
        }

        $peserta = Peserta::find()->where(['token'=>$session->get('token')])->one();

        $listPertanyaan1 = $peserta->prodiTujuan1->pertanyaanProdis;
        $listPertanyaan2 = $peserta->prodiTujuan2->pertanyaanProdis;
        $listPertanyaan3 = $peserta->prodiTujuan3->pertanyaanProdis;
        
        $arr = [$listPertanyaan1, $listPertanyaan2, $listPertanyaan3];

        $hasil = [];
        foreach ($arr as $key => $value) {
            # code...
            $tmp = $arr[$key];
            foreach($tmp as $p)
            {
                $hasil[$p->jenis_pertanyaan_id] = $p;
            }
        }

        $results = [];
        foreach($hasil as $p)
        {
            foreach($p->jenisPertanyaan->subbagians as $sub)
            {

                $total = 0;
                foreach($sub->pertanyaans as $per)
                {
                    $jwbn = Jawaban::find()->where([
                        'peserta_id' => $peserta->id,
                        'pertanyaan_id' => $per->id
                    ])->one();
                    $total += !empty($jwbn) ? $jwbn->poin : 0;
                }

                $results[$p->id][$sub->id] = $total;
            }
        }
        return $this->render('hasil',[
            'results' => $results,
            'peserta' => $peserta,
            'hasil' => $hasil
        ]);        
    }

    public function actionSelesai()
    {
        $session = Yii::$app->session;
        $session->remove('token');

        $this->refresh();
        Yii::$app->session->setFlash('success', "Terima kasih. Anda telah selesai mengikuti ujian online seleksi mahasiswa baru Universitas Darussalam Gontor. Semoga sukses selalu.");
        return $this->redirect(['site/test']);
    }

    public function actionSoal($id)
    {
        $quiz = Quiz::find()->where(['buka' => 'Y'])->one();
        $sd = $quiz->time_start;
        $ed = $quiz->time_end;

        $session = Yii::$app->session;
        
        $tglnow = date('Y-m-d H:i:s');

        if($tglnow > $ed)
        {
            $session->remove('token');   
        }
        if(!$session->has('token'))
        {
            $this->refresh();
            Yii::$app->session->setFlash('warning', "Maaf, sesi Anda telah habis.");
            return $this->redirect(['site/test']);
        }
        // $jenisPertanyaan = JenisPertanyaan::findOne($id);
        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token'=>$client_token];
        $params = [
            'id' => $id,
            'quiz_id' => $quiz->id
        ];
        $response = $client->get('/ujian/soal/jenis', $params,$headers)->send();
        
        $results = [];

        if ($response->isOk) {
            $results = $response->data['values'];
            // print_r($results);exit;
            
        }
        $session = Yii::$app->session;

        $listJawabans = [];

        $peserta = Peserta::find()->where(['token'=>$session->get('token')])->one();

        foreach($results as $q => $res)
        {
            foreach($res['items'] as $model)
            {
                $model = (object)$model;
                $qid = $model->id;
                $pid = $peserta->id;

                $params = [
                    'qid' => $qid,
                    'pid' => $pid
                ];
                $response = $client->get('/ujian/jawaban', $params,$headers)->send();
                
                if ($response->isOk) {
                    $tmp = $response->data['values'];
                    
                    $listJawabans[$pid][$qid] = $tmp[0]['jawaban'];
                    // print_r($tmp);exit;
                }

            }
        }



        if(!empty($_POST['btn-simpan']))
        {
            foreach($results as $q => $res)     
            {
                foreach($res['items'] as $model)
                {
                    
                    $model = (object)$model;
                    $ans = !empty($_POST['ans'.$model->id]) ? $_POST['ans'.$model->id] : null;

                    $params = [
                        'peserta_id' => $peserta->id,
                        'pertanyaan_id' => $model->id,
                        'jawaban' => $ans,
                        'poin' => $model->jawaban_benar == $ans ? $model->poin : '0'
                    ];
                    $response = $client->post('/ujian/jawaban/sync', $params,$headers)->send();
                    // print_r($_POST);exit;
                    $results = [];

                    if ($response->isOk) {
                        $results = $response->data['values'];
                        // print_r($results);exit;
                        
                    }
                    
                    
                    
                }
            }
            $this->refresh();
            Yii::$app->session->setFlash('success', "Terima kasih. Anda telah mengerjakan soal ".$jenisPertanyaan->nama.". Silakan mengerjakan soal lainnya");
            return $this->redirect(['ujian']);
        }

        return $this->render('soal', [
            'results' => $results,
            'peserta' => $peserta,
            'quiz' => $quiz,
            'listJawabans' => $listJawabans
        ]);
    }

    public function actionUjian()
    {
        // $this->layout = 'quiz';

        $quiz = Quiz::find()->where(['buka' => 'Y'])->one();
        $session = Yii::$app->session;

        $peserta = Peserta::find()->where(['token'=>$session->get('token')])->one();
        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token'=>$client_token];
        
        $params = [
            'id1' => $peserta->prodiTujuan1->id_prodi,
            'id2' => $peserta->prodiTujuan2->id_prodi,
            'id3' => $peserta->prodiTujuan3->id_prodi,
            'quiz_id' => $quiz->id
        ];
        $response = $client->get('/ujian/soal/jenis/prodi', $params,$headers)->send();
        
        $results = [];

        if ($response->isOk) {
            $results = $response->data['values'];
            // print_r($results);exit;
            
        }

        if(!$session->has('token'))
        {
            $this->refresh();
            Yii::$app->session->setFlash('warning', "Maaf, sesi Anda telah habis.");
            return $this->redirect(['site/test']);
        }


        return $this->render('ujian', [
            'peserta' => $peserta,
            'quiz' => $quiz,
            'results' => $results
        ]);
    }

    /**
     * Lists all Pertanyaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PertanyaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pertanyaan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pertanyaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pertanyaan();  
        $quiz = Quiz::find()->where(['buka' => 'Y'])->one();
        $model->quiz_id = $quiz->id;
        $current_image = $model->buktiFile;
        if ($model->load(Yii::$app->request->post())) 
        {
            $image = UploadedFile::getInstance($model, 'buktiFile');
            
         

            if($model->validate())
            {  
                if(!empty($image))
                {
                    $path = 'uploads/'. $model->subbagian_id;
                    FileHelper::createDirectory($path);              
                    $namafile = $path.'/'. $model->subbagian_id.'_'.mt_rand(1,100000).'.' . $image->extension;
                    $image->saveAs($namafile);

                    $model->foto_path = $namafile;
                    $model->buktiFile = $model->foto_path;

                }
                else{
                    $model->buktiFile = $current_image;
                }
                

                $model->save();
                Yii::$app->session->setFlash('success', "Data tersimpan");
                return $this->redirect(['index']);    
            }



            

               
        }
           

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pertanyaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $current_image = $model->buktiFile;
        if ($model->load(Yii::$app->request->post())) 
        {
            $image = UploadedFile::getInstance($model, 'buktiFile');
            
         

            if($model->validate())
            {  
                if(!empty($image))
                {
                    $path = 'uploads/'. $model->subbagian_id;
                    FileHelper::createDirectory($path);              
                    $namafile = $path.'/'. $model->subbagian_id.'_'.mt_rand(1,100000).'.' . $image->extension;
                    $image->saveAs($namafile);
                    $model->foto_path = $namafile;
                    $model->buktiFile = $model->foto_path;

                }
                else{
                    $model->buktiFile = $current_image;
                }
                
                if($model->deleteFile)
                {
                    if(file_exists(Yii::$app->basePath.'/../'.$model->foto_path))
                    {   
                        unlink(Yii::$app->basePath.'/../'.$model->foto_path);
                    
                        $model->foto_path = null;
                    }
                }
                $model->save();
                Yii::$app->session->setFlash('success', "Data tersimpan");
                return $this->redirect(['update','id'=>$id]);    
            }



            

               
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pertanyaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pertanyaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pertanyaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pertanyaan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

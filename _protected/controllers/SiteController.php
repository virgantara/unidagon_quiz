<?php
namespace app\controllers;

use app\models\Pertanyaan;
use app\models\User;
use app\models\LoginForm;
use app\models\AccountActivation;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\ContactForm;
use yii\httpclient\Client;
use yii\helpers\Html;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Peserta;
use Da\QrCode\QrCode;
use Yii;

/**
 * Site controller.
 * It is responsible for displaying static pages, logging users in and out,
 * sign up and account activation, and password reset.
 */
class SiteController extends Controller
{

    public $successUrl = '';
    /**
     * Returns a list of behaviors that this component should behave as.
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup','test'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Declares external actions for the controller.
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
                'successUrl' => $this->successUrl
            ],
        ];
    }

    public function successCallback($client)
    {
        $attributes = $client->getUserAttributes();
        $user = \app\models\User::find()
            ->where([
                'email'=>$attributes['email'],
            ])
            ->one();
        if(!empty($user)){
            Yii::$app->user->login($user);
        }
        else{
            // print_r($client);exit;
            //Simpen disession attribute user dari Google
            $session = Yii::$app->session;
            $session['attributes']=$attributes;
            
            $user = new User();

            $user->username = $attributes['email'];
            $user->email = $attributes['email'];
            $user->setPassword($attributes['email']);
            $user->generateAuthKey();
            $user->access_role = 'member';
            $user->created_at = strtotime(date('Y-m-d H:i:s'));
            $user->updated_at = strtotime(date('Y-m-d H:i:s'));
            $user->status = User::STATUS_ACTIVE;//$this->status;
            $user->save();
            $auth = Yii::$app->authManager;
            $role = $auth->getRole($user->access_role);
            $info = $auth->assign($role, $user->getId());
            $this->successUrl = \yii\helpers\Url::to(['peserta/create']);
            Yii::$app->user->login($user);

        }   
    }

//------------------------------------------------------------------------------------------------//
// STATIC PAGES
//------------------------------------------------------------------------------------------------//

    /**
     * Displays the index (home) page.
     * Use it in case your home page contains static content.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect(['/site/test']);
        }

        else
        {

            if(Yii::$app->user->can('admin') || Yii::$app->user->can('operatorCabang'))
            {
                return $this->render('index',[
                    'data'=>$out
                ]);
            }
            else if(Yii::$app->user->can('member'))
            {
                $tmp = Peserta::find()->where(['email'=>Yii::$app->user->identity->email])->one();

                if(empty($tmp))
                    $this->redirect(['peserta/create']);
                else
                {
                    return $this->render('index',[
                        'data'=>$out
                    ]);
                }
            }

            
        }
    }

    public function actionTest()
    {
        $this->layout = 'quiz';
        $model = new Peserta;

        if ($model->load(Yii::$app->request->post())) {
            $m = Peserta::find()->where(['token'=>$model->token])->one();

            if(!empty($m))
            {
                $session = Yii::$app->session;
                $session->set('token',$m->token);

                return $this->redirect(['pertanyaan/ujian']);
            }
        }

        return $this->render('test',[
            'model'=>$model
        ]);
    }

    /**
     * Displays the about static page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays the contact static page and sends the contact email.
     *
     * @return string|\yii\web\Response
     */
    public function actionContact()
    {
        $model = new ContactForm();

        if (!$model->load(Yii::$app->request->post()) || !$model->validate()) {
            return $this->render('contact', ['model' => $model]);
        }

        if (!$model->sendEmail(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('error', Yii::t('app', 'There was some error while sending email.'));
            return $this->refresh();
        }

        Yii::$app->session->setFlash('success', Yii::t('app', 
            'Thank you for contacting us. We will respond to you as soon as possible.'));
        
        return $this->refresh();
    }

//------------------------------------------------------------------------------------------------//
// LOG IN / LOG OUT / PASSWORD RESET
//------------------------------------------------------------------------------------------------//

    /**
     * Logs in the user if his account is activated,
     * if not, displays appropriate message.
     *
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        $this->layout = 'default';
        // user is logged in, he doesn't need to login
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        // get setting value for 'Login With Email'
        $lwe = Yii::$app->params['lwe'];

        // if 'lwe' value is 'true' we instantiate LoginForm in 'lwe' scenario
        $model = $lwe ? new LoginForm(['scenario' => 'lwe']) : new LoginForm();

        // monitor login status
        $successfulLogin = true;

        // posting data or login has failed
        if (!$model->load(Yii::$app->request->post()) || !$model->login()) {
            $successfulLogin = false;
        }



        // if user's account is not activated, he will have to activate it first
        if ($model->status === User::STATUS_INACTIVE && $successfulLogin === false) {
            Yii::$app->session->setFlash('error', Yii::t('app', 
                'You have to activate your account first. Please check your email.'));
            return $this->refresh();
        } 

        // if user is not denied because he is not active, then his credentials are not good
        if ($successfulLogin === false) {
            return $this->render('login', ['model' => $model]);
        }

        // login was successful, let user go wherever he previously wanted
        return $this->goBack();
    }

    /**
     * Logs out the user.
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        
        Yii::$app->user->logout();

        return $this->goHome();
    }

/*----------------*
 * PASSWORD RESET *
 *----------------*/

    /**
     * Sends email that contains link for password reset action.
     *
     * @return string|\yii\web\Response
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'default';
        $model = new PasswordResetRequestForm();

        if (!$model->load(Yii::$app->request->post()) || !$model->validate()) {
            return $this->render('requestPasswordResetToken', ['model' => $model]);
        }

        if (!$model->sendEmail()) {
            Yii::$app->session->setFlash('error', Yii::t('app', 
                'Sorry, we are unable to reset password for email provided.'));
            return $this->refresh();
        }

        Yii::$app->session->setFlash('success', Yii::t('app', 'Silakan cek inbox email Anda. Jika tidak ada, mohon cek spam Anda'));

        return $this->goHome();
    }

    /**
     * Resets password.
     *
     * @param  string $token Password reset token.
     * @return string|\yii\web\Response
     *
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if (!$model->load(Yii::$app->request->post()) || !$model->validate() || !$model->resetPassword()) {
            return $this->render('resetPassword', ['model' => $model]);
        }

        Yii::$app->session->setFlash('success', Yii::t('app', 'New password was saved.'));

        return $this->goHome();      
    }    

//------------------------------------------------------------------------------------------------//
// SIGN UP / ACCOUNT ACTIVATION
//------------------------------------------------------------------------------------------------//

    private function sendmail($email)
    {
        $emailTemplate = $this->renderPartial('email');
       Yii::$app->mailer->compose()
        ->setTo($email)
        ->setFrom([Yii::$app->params['supportEmail'] => 'PMB UNIDA Gontor'])
        ->setSubject('Registrasi UNIDA Gontor')
        ->setHtmlBody($emailTemplate)
        ->send();
    }

    // public function actionTest(){

    //     $this->sendmail('vinux.edu@gmail.com');
    //     exit;
        // $api_baseurl = Yii::$app->params['api_baseurl'];
        // $client = new Client(['baseUrl' => 'https://www.googleapis.com/urlshortener/v1']);
        // // $client_token = Yii::$app->params['client_token'];
        // $headers = [
        //     // 'x-access-token'=>$client_token,
        //     'Content-Type' => 'Application/json'
        // ];

        // $params = [
        //     'form_params' => [
        //         'longUrl' => 'http://local.admisi.com/site/accc',
        //         'key' => 'AIzaSyA0eFX3IXy0NM9WIXsgb46ynB77c223skU'
        //     ],
        //     'verify' => false
        // ];
        // $response = $client->post('/url', $params,$headers)->send();
        
        // $results = [];
        
        // if ($response->isOk) {
        //     print_r($response->data);
            
        // }
        
        // else{
        //     print_r($response);
        // }
        // exit;
        // $client = new \Google_Client();
        // $client->setClientId("724846926093-q9no7n4907j5mbsbk9kn2q9r8rgonnsr.apps.googleusercontent.com");
        // $client->setClientSecret("W5EG7pJ_aW-T8n1oEFaWp5ea");
        // $client->setRedirectUri(Yii::$app->params['redirectUri']);
        // $client->setAccessType('offline');
        // $client->setApprovalPrompt('force');
     
        // $client->addScope("https://mail.google.com/");
        // $client->addScope("https://www.googleapis.com/auth/gmail.compose");
        // $client->addScope("https://www.googleapis.com/auth/gmail.modify");
        // $client->addScope("https://www.googleapis.com/auth/gmail.readonly");
        // $session = Yii::$app->session;
         
        // if (isset($_REQUEST['code'])) {
        //     //land when user authenticated
        //     $code = $_REQUEST['code'];
        //     $client->authenticate($code);
            
        //     $session->set('gmail_access_token', $client->getAccessToken());    
        //     // $_SESSION['gmail_access_token'] = ;
             
        //     header("Location: http://local.admisi.com/site/test");
        // }
         
        //$isAccessCodeExpired = $client->isAccessTokenExpired();
         
         // print_r($_SESSION['gmail_access_token']);exit;
        //if (isset($_SESSION['gmail_access_token']) &amp;&amp; !empty($_SESSION['gmail_access_token']) &amp;&amp; $isAccessCodeExpired != 1) {
        


        // $p = new Peserta;
        // $p->nama = 'Oddy';
        // $p->tempat_lahir = 'aa';
        // $p->tanggal_lahir = '2020-02-02';
        // $p->jk='L';
        // $p->agama_id = 1;
        // $p->status_warga_negara = 'WNI';
        // $p->nik = '23912321';
        // $p->kecamatan_id = 3312150;
        // $p->email = 'vinux.edu@gmail.com';
        // $rna = Yii::$app->params['rna'];

        // // if 'rna' value is 'true', we instantiate SignupForm in 'rna' scenario
        // $signup = $rna ? new SignupForm(['scenario' => 'rna']) : new SignupForm();

        // $signup->email = $p->email;
        // $connection = \Yii::$app->db;
        // $transaction = $connection->beginTransaction();
        // try 
        // {
        //     if ($p->validate()) 
        //     {
        //         $p->save();
        //         $signup->email = 'vinux.edu@gmail.com';
        //         $signup->username = 'oddy';
        //         $signup->password = 'oddyoddyoddy';//$_POST['SignupForm']['password']; 
        //         if (empty($signup->email) || empty($signup->username)) {
        //             // else{
        //             print_r('error signup');exit;
        //         // }
        //             // exit;
        //             // return $this->render('signup', [
        //             //     'p' => $p,'signup'=>$signup
        //             // ]);  
        //         }

                

        //         $user = $signup->signup();
        //         // print_r($user);exit;
        //         if (!$user) {
        //             // display error message to user
        //             Yii::$app->session->setFlash('error', Yii::t('app', 'We couldn\'t sign you up, please contact us.'));
        //             return $this->refresh();
        //         }

        //         // $transaction->commit();
        //         // user is saved but activation is needed, use signupWithActivation()
        //         if ($user->status === User::STATUS_INACTIVE) {
                //     if ($session->has('gmail_access_token')) 
                //     {
                // //gmail_access_token setted;
                 
                //         $boundary = uniqid(rand(), true);
                     
                //         $client->setAccessToken($session->get('gmail_access_token'));            
                //         $objGMail = new \Google_Service_Gmail($client);
                         
                //         $subjectCharset = $charset = 'utf-8';
                //         $strToMailName = $p->nama;
                //         $strToMail = $user->email;
                //         $strSesFromName = 'IT Service UNIDA Gontor';
                //         $strSesFromEmail = 'noreply@unida.gontor.ac.id';
                //         $strSubject = 'Aktivasi Token '.date('Y-m-d H:i:s');
                     
                //         $strRawMessage .= 'To: ' . ($strToMailName . " <" . $strToMail . ">") . "\r\n";
                //         $strRawMessage .= 'From: '.($strSesFromName . " <" . $strSesFromEmail . ">") . "\r\n";
                     
                //         $strRawMessage .= 'Subject: =?' . $subjectCharset . '?B?' . base64_encode($strSubject) . "?=\r\n";
                //         $strRawMessage .= 'MIME-Version: 1.0' . "\r\n";
                //         // $strRawMessage .= 'Content-type: Multipart/Alternative; boundary="' . $boundary . '"' . "\r\n";
                   
                //         // $strRawMessage .= "\r\n--{$boundary}\r\n";
                //         // $strRawMessage .= 'Content-Type: text/plain; charset=' . $charset . "\r\n";
                //         // $strRawMessage .= 'Content-Transfer-Encoding: 7bit' . "\r\n\r\n";
                        
                //         // $strRawMessage .= "boundary={$boundary}\r\n";
                //         $strRawMessage .= 'Content-Type: text/html;boundary="'.$boundary.'"; charset=' . $charset . "\r\n";

                //         $strRawMessage .= 'Content-Transfer-Encoding: quoted-printable' . "\r\n\r\n";
                        
                //         $message = 'Assalamualaikum, Testing. Silakan klik link berikut untuk aktivasi akun Anda.';
                //         $message .= Yii::$app->urlManager->createAbsoluteUrl(['site/activate-account']).'?token='.$user->account_activation_token;

                //         $strRawMessage .= $message. "\r\n";
                //         //Send Mails
                //         //Prepare the message in message/rfc822
                //         try {
                //             // The message needs to be encoded in Base64URL
                //             $mime = rtrim(strtr(base64_encode($strRawMessage), '+/', '-_'), '=');
                //             $msg = new \Google_Service_Gmail_Message();
                //             $msg->setRaw($mime);
                //             $objSentMsg = $objGMail->users_messages->send("me", $msg);
                     
                //             print_r('Message sent object');
                //             // print_r($objSentMsg);
                     
                //         } catch (Exception $e) {
                //             print_r($e->getMessage());
                //             unset($_SESSION['gmail_access_token']);
                //         }
                //     }
                //     else {
                //         // Failed Authentication
                //         if (isset($_REQUEST['error'])) {
                //             //header('Location: ./index.php?error_code=1');
                //             echo "error auth";
                //         }
                //         else{
                //             // Redirects to google for User Authentication
                //             $authUrl = $client->createAuthUrl();
                //             header("Location: $authUrl");
                //         }
                //     }
                    // $this->signupWithActivation($signup, $user);

    //                 $url = Yii::$app->urlManager->createAbsoluteUrl(['site/activate-account','token'=>$user->account_activation_token]);
    //                 $qrCode = (new QrCode($url))
    //                     ->setSize(250)
    //                     ->setMargin(5);
    //                     // ->useForegroundColor(51, 153, 255);

    //                 // now we can display the qrcode in many ways
    //                 // saving the result to a file:

    //                 $qrCode->writeFile(__DIR__ . '/code.png'); // writer defaults to PNG when none is specified

    //                 // display directly to the browser 
    //                 // header('Content-Type: '.$qrCode->getContentType());
    //                 // echo $qrCode->writeString();
    //                 // echo '<img src="' . $qrCode->writeDataUri() . '">';
    //                 // exit;
    //                 return $this->refresh();
    //             }

    //             // now we will try to log user in
    //             // if login fails we will display error message, else just redirect to home page
            
    //             if (!Yii::$app->user->login($user)) {
    //                 // display error message to user
    //                 Yii::$app->session->setFlash('warning', Yii::t('app', 'Please try to log in.'));

    //                 // log this error, so we can debug possible problem easier.
    //                 Yii::error('Login after sign up failed! User '.Html::encode($user->username).' could not log in.');
    //             }

                
    //         }

    //         else{
    //             print_r($p->getErrors());
    //             echo 'Error P';
    //             throw new Exception;
                
                
    //         }

           
            
    //     } catch (\Exception $e) {
    //         print_r($e->getMessage());
    //         $transaction->rollBack();
    //         // throw $e;
    //     } catch (\Throwable $e) {
    //         print_r($e->getMessage());
    //         $transaction->rollBack();
    //         // throw $e;
    //     }
    //     die();
    // }

    /**
     * Signs up the user.
     * If user need to activate his account via email, we will display him
     * message with instructions and send him account activation email with link containing account activation token. 
     * If activation is not necessary, we will log him in right after sign up process is complete.
     * NOTE: You can decide whether or not activation is necessary, @see config/params.php
     *
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {  


        $model = new Peserta();
        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token'=>$client_token];
        $response = $client->get('/p/list', [],$headers)->send();
        
        $listProdi = [];
        
        if ($response->isOk) {
            $tmp = $response->data['values'];

            foreach($tmp as $item){
                $listProdi[$item['kode_prodi']] = $item['nama_prodi'];
            }
        }

        $response = $client->get('/k/list', [],$headers)->send();
        
        $listKampus = [];
        
        if ($response->isOk) {
            $tmp = $response->data['values'];

            foreach($tmp as $item){
                $listKampus[$item['kode_kampus']] = $item['nama_kampus'];
            }
        }

        // $this->layout = 'default';
        // get setting value for 'Registration Needs Activation'
        $rna = Yii::$app->params['rna'];

        // if 'rna' value is 'true', we instantiate SignupForm in 'rna' scenario
        $signup = $rna ? new SignupForm(['scenario' => 'rna']) : new SignupForm();

        $signup->email = $model->email;
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try 
        {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

            }

            $signup->email = $model->email;
            $signup->username = $_POST['SignupForm']['username'];
            $signup->password = $_POST['SignupForm']['password']; 
            if (empty($signup->email) || empty($signup->username)) {
                // else{
                // print_r($signup);exit;
            // }
                // exit;
                return $this->render('signup', [
                    'model' => $model,'signup'=>$signup
                ]);  
            }

            

            $user = $signup->signup();

            if (!$user) {
                // display error message to user
                Yii::$app->session->setFlash('error', Yii::t('app', 'We couldn\'t sign you up, please contact us.'));
                return $this->refresh();
            }

            $transaction->commit();
            // user is saved but activation is needed, use signupWithActivation()
            if ($user->status === User::STATUS_INACTIVE) {
                $url = Yii::$app->urlManager->createAbsoluteUrl(['site/activate-account','token'=>$user->account_activation_token]);
                $qrCode = (new QrCode($url))
                    ->setSize(250)
                    ->setMargin(5);
                    // ->useForegroundColor(51, 153, 255);

                // now we can display the qrcode in many ways
                // saving the result to a file:

                $qrCode->writeFile(__DIR__ . '/code.png'); // writer defaults to PNG when none is specified

                // display directly to the browser 
                // header('Content-Type: '.$qrCode->getContentType());
                // echo $qrCode->writeString();
                $msg = 'Terima kasih sudah mendaftar. Scan kode berikut untuk aktivasi atau klik link di bawahnya.<br>';
                
                $msg .= '<img src="' . $qrCode->writeDataUri() . '"><br>';
                $msg .= '<a href="'.$url.'">';
                Yii::$app->session->setFlash('success', Yii::t('app', $msg));
                // $this->signupWithActivation($signup, $user);
                return $this->refresh();
            }

            // now we will try to log user in
            // if login fails we will display error message, else just redirect to home page
        
            if (!Yii::$app->user->login($user)) {
                // display error message to user
                Yii::$app->session->setFlash('warning', Yii::t('app', 'Please try to log in.'));

                // log this error, so we can debug possible problem easier.
                Yii::error('Login after sign up failed! User '.Html::encode($user->username).' could not log in.');
            }

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        
        // if validation didn't pass, reload the form to show errors
        

        // try to save user data in database, if successful, the user object will be returned
        
                      
        return $this->goHome();
    }

    /**
     * Tries to send account activation email.
     *
     * @param $model
     * @param $user
     */
    private function signupWithActivation($model, $user)
    {
        // sending email has failed
        if (!$model->sendAccountActivationEmail($user)) {
            // display error message to user
            Yii::$app->session->setFlash('error', Yii::t('app', 
                'We couldn\'t send you account activation email, please contact us.'));

            // log this error, so we can debug possible problem easier.
            Yii::error('Signup failed! User '.Html::encode($user->username).' could not sign up. 
                Possible causes: verification email could not be sent.');


        }

        // everything is OK
        Yii::$app->session->setFlash('success', Yii::t('app', 'Hello').' '.Html::encode($user->username). '. ' .
            Yii::t('app', 'To be able to log in, you need to confirm your registration. 
                Please check your email, we have sent you a message.'));
    }

/*--------------------*
 * ACCOUNT ACTIVATION *
 *--------------------*/

    /**
     * Activates the user account so he can log in into system.
     *
     * @param  string $token
     * @return \yii\web\Response
     *
     * @throws BadRequestHttpException
     */
    public function actionActivateAccount($token)
    {
        try {
            $user = new AccountActivation($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if (!$user->activateAccount()) {
            Yii::$app->session->setFlash('error', Html::encode($user->username). Yii::t('app', 
                ' your account could not be activated, please contact us!'));
            return $this->goHome();
        }

        Yii::$app->session->setFlash('success', Yii::t('app', 'Success! You can now log in.').' '.
            Yii::t('app', 'Thank you').' '.Html::encode($user->username).' '.Yii::t('app', 'for joining us!'));

        return $this->redirect('login');
    }
}

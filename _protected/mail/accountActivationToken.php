<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate-account', 
    'token' => $user->account_activation_token]);
?>

Hello <?= Html::encode($user->username) ?>,

Follow this link to activate your account:
<?php echo $resetLink;?>http://local.admisi.com/site/activate-account&token=<?=$user->account_activation_token;?>
<?= Html::a('Please, click here to activate your account. ', $resetLink) ?>
<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin([
    	'options' => [
    		'class' => 'form-horizontal'
    	]
    ]); 

    echo $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']);
    
    ?>
     <?= "<?php";?>    
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
     <?= "?>";?>

        <div class="alert alert-<?="<?=\$key?>"?>"><?="<?=\$message?>"?></div>
     <?= "<?php";?>
     }
     <?= "?>";?>
<?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes)) {
        $tableSchema = $generator->getTableSchema();
        $column = $tableSchema->columns[$attribute];

        if(in_array($column->name, ['created_at','updated_at'])) continue;
        ?>
    <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right"><?= Inflector::humanize($column->name);?></label>
            <div class="col-sm-9">
        <?php
        echo "    <?= " . $generator->generateActiveField($attribute). "->label(false) ?>\n\n";
         ?>
            </div>
        </div>
        <?php
    }
} ?>
    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
        <?= "<?= " ?>Html::submitButton(<?= $generator->generateString('Save') ?>, ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-form">

    <?php $form = ActiveForm::begin([
    	'options' => [
    		'class' => 'form-horizontal'
    	]
    ]); ?>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Name</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'name',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Type</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'type',['options' => ['tag' => false]])->textInput()->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Description</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'description',['options' => ['tag' => false]])->textarea(['rows' => 6])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Rule name</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'rule_name',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Data</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'data',['options' => ['tag' => false]])->textarea(['rows' => 6])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Created at</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'created_at',['options' => ['tag' => false]])->textInput()->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Updated at</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'updated_at',['options' => ['tag' => false]])->textInput()->label(false) ?>

            </div>
        </div>
            <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\MProdi;
use app\models\JenisPertanyaan;

/* @var $this yii\web\View */
/* @var $model app\models\PertanyaanProdi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pertanyaan-prodi-form">

    <?php $form = ActiveForm::begin([
    	'options' => [
    		'class' => 'form-horizontal'
    	]
    ]); 

    echo $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']);
    
    ?>
     <?php    
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
     ?>
        <div class="alert alert-<?=$key?>"><?=$message?></div>
     <?php     }
     ?> 

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Prodi</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'prodi_id',['options' => ['tag' => false]])->dropDownList(ArrayHelper::map(MProdi::find()->all(),'id_prodi','nama_prodi'))->label(false) ?>

            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Jenis pertanyaan</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'jenis_pertanyaan_id',['options' => ['tag' => false]])->dropDownList(ArrayHelper::map(JenisPertanyaan::find()->all(),'id','nama'),['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
            <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PertanyaanProdi */

$this->title = 'Create Pertanyaan Prodi';
$this->params['breadcrumbs'][] = ['label' => 'Pertanyaan Prodis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pertanyaan-prodi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

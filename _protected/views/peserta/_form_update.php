<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Peserta */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
    .ui-state-focus {
  background: none !important;
  background-color: #4F99C6 !important;
  /*border: none !important;*/
}
</style>
<div class="peserta-form">

    <?php $form = ActiveForm::begin([
    	'options' => [
    		'class' => 'form-horizontal'
    	]
    ]); ?>
    <?= $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']) ?>
<div class="panel panel-primary">
    <div class="panel-heading">Data Calon Mahasiswa</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Nama *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'nama',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Tempat lahir *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'tempat_lahir',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Tanggal lahir *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'tanggal_lahir',['options' => ['tag' => false]])->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Input tanggal lahir ...'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])->label(false);
                 ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Jenis Kelamin *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'jk',['options' => ['tag' => false]])->radioList(['L'=>'Laki-laki','P'=>'Perempuan'],['maxlength' => true])->label(false) ?>

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">NIK *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'nik',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">NISN</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'nisn',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Nama ibu kandung *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'nama_ibu_kandung',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
          <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Agama</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'agama_id',['options' => ['tag' => false]])->dropDownList(ArrayHelper::map(\app\models\Agama::find()->all(),'id','nama'))->label(false) ?>

            </div>
        </div>
    </div></div>
        
<div class="panel panel-primary">
    <div class="panel-heading">Alamat</div>
    <div class="panel-body">
      
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Status warga negara *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'status_warga_negara',['options' => ['tag' => false]])->radioList(['WNI'=>'WNI','WNA'=>'WNA'])->label(false) ?>

            </div>
        </div>
                
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Jalan</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'jalan',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Dusun</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'dusun',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kelurahan *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'kelurahan',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kecamatan *</label>
            <div class="col-sm-6">
                            
    <?php 
            AutoComplete::widget([
    'name' => 'nama_kecamatan',
    'id' => 'nama_kecamatan',
    'clientOptions' => [
    'source' => Url::to(['kecamatan/ajax-get-kecamatan']),
    'autoFill'=>true,
    'minLength'=>'1',
    'select' => new JsExpression("function( event, ui ) {
        $('#peserta-kecamatan_id').val(ui.item.id);
        
     }")],
    'options' => [
        // 'size' => '40'
    ]
 ]); 
 ?>
 <input type="text" id="nama_kecamatan" name="nama_kecamatan" class="form-control" />
            <?= $form->field($model, 'kecamatan_id',['options' => ['tag' => false]])->hiddenInput()->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">RT</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'rt',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">RW</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'rw',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kodepos</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'kodepos',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
    </div></div>
    <div class="panel panel-primary">
    <div class="panel-heading">Kontak</div>
    <div class="panel-body">

                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Telepon</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'telepon',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Email *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'email',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Hp (Nomor Whatsapp) *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'hp',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
    </div></div>
     <div class="panel panel-primary">
    <div class="panel-heading">Lainnya</div>
    <div class="panel-body">           
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Lulusan Pesantren? *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'is_pesantren',['options' => ['tag' => false]])->radioList(['1'=>'Ya','0'=>'Tidak'])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Nama pesantren</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'nama_pesantren',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Tahun lulus</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'tahun_lulus',['options' => ['tag' => false]])->textInput()->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Lama pendidikan</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'lama_pendidikan',['options' => ['tag' => false]])->textInput()->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Takhassus</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'takhassus',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
    </div></div>
    <div class="panel panel-primary">
    <div class="panel-heading">Riwayat Pendidikan</div>
    <div class="panel-body">
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">SD *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'sd',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">SMP *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'smp',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">SMA *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'sma',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
    </div></div>
    <div class="panel panel-primary">
    <div class="panel-heading">Data Ayah</div>
    <div class="panel-body">
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Nama *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'nama_ayah',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">No KTP/NIK *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'nik_ayah',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Tanggal lahir *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'tanggal_lahir_ayah',['options' => ['tag' => false]])->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Input tanggal lahir ...'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])->label(false);
                 ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Pendidikan * </label>
            <div class="col-sm-6">
            <?= $form->field($model, 'pendidikan_ayah_id',['options' => ['tag' => false]])->dropDownList(ArrayHelper::map(\app\models\Pendidikan::find()->all(),'id','nama'))->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Pekerjaan  *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'pekerjaan_ayah_id',['options' => ['tag' => false]])->dropDownList(ArrayHelper::map(\app\models\Pekerjaan::find()->all(),'id','nama'))->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Penghasilan * </label>
            <div class="col-sm-6">
            <?= $form->field($model, 'penghasilan_ayah_id',['options' => ['tag' => false]])->dropDownList(ArrayHelper::map(\app\models\Penghasilan::find()->all(),'id','nama'))->label(false) ?>

            </div>
        </div>
              
    </div></div>
<div class="panel panel-primary">
    <div class="panel-heading">Informasi </div>
    <div class="panel-body">
                     
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Prodi tujuan 1 *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'prodi_tujuan1',['options' => ['tag' => false]])->dropDownList($listProdi)->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Prodi tujuan 2 *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'prodi_tujuan2',['options' => ['tag' => false]])->dropDownList($listProdi)->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Prodi tujuan 3 *</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'prodi_tujuan3',['options' => ['tag' => false]])->dropDownList($listProdi)->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Lulusan KMI?</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'is_kmi',['options' => ['tag' => false]])->radioList(['1'=>'Ya','0'=>'Tidak'])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Rencana Studi di UNIDA Gontor</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'lama_rencana_kuliah',['options' => ['tag' => false]])->radioList(['1'=>'1 Tahun','4'=>'4 Tahun (S1)'])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Di kampus mana Anda akan mengikuti perkuliahan?</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'kampus_id',['options' => ['tag' => false]])->dropDownList($listKampus)->label(false) ?>

            </div>
        </div>
       </div></div>
            <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Peserta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="peserta-form">

    <?php $form = ActiveForm::begin([
    	'options' => [
    		'class' => 'form-horizontal'
    	]
    ]); ?>
    <?php
     foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
     } ?>
    <?= $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']) ?>
    <div class="panel panel-primary">
        <div class="panel-heading">Data Calon Mahasiswa</div>
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right">Nama *</label>
                <div class="col-sm-6">
                <?= $form->field($model, 'nama',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right">Token Ujian *</label>
                <div class="col-sm-6">
                <?= $form->field($model, 'token',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

                </div>
            </div>    
            <div class="col-sm-offset-3 clearfix">
                <?= Html::button('Generate Token', ['class' => 'btn btn-primary','id'=>'btn-generate']) ?>
                <?= Html::submitButton('<i class="fa fa-save"></i> Simpan', ['class' => 'btn btn-success']) ?>
            </div>  
        </div>
            

    </div>
    <?php ActiveForm::end();?>
</div>

<?php

$this->registerJs(' 

function generate_token(length){
    //edit the token allowed characters
    var a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    var b = [];  
    for (var i=0; i<length; i++) {
        var j = (Math.random() * (a.length-1)).toFixed(0);
        b[i] = a[j];
    }
    return b.join("");
}

$("#btn-generate").click(function(e){
    e.preventDefault();

    $("#'.Html::getInputId($model, 'token').'").val(generate_token(6));
});

    ', \yii\web\View::POS_READY);

?>

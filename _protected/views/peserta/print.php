<?php

$top = 'border-top: 0.5px solid black;';
$left = 'border-left: 0.5px solid black;';
$right = 'border-right: 0.5px solid black;';
$bottom = 'border-bottom: 0.5px solid black;';
?>
<table width="100%" style="padding-top:5px" >
    <tr>
        <th><img src="<?=Yii::$app->view->theme->baseUrl;?>/images/logo_unida_left.png" width="180"></th>
    </tr>
</table>
<table width="100%" border="0" cellpadding="4" cellspacing="0" style="padding-bottom: 5px">
    <tr>
        <td colspan="4" style="text-align: center;<?=$bottom;?>"><strong>Data Diri</strong></td>
    </tr>
    <tr>
        <td width="15%" style="">Nama</td>
        <td width="35%" style="">: <?=$model->nama;?></td>
        <td width="15%" style="">Status WN</td>
        <td width="35%" style="">: <?=$model->status_warga_negara;?></td>
    </tr>
    <tr>
        <td width="15%" style="">TTL</td>
        <td width="35%" style="">: <?=$model->tempat_lahir.', '.$model->tanggal_lahir;?></td>
        <td width="15%" style="">Jalan</td>
        <td width="35%" style="">: <?=$model->jalan;?></td>
    </tr>
    <tr>
        <td width="15%" style="">Jenis Kelamin</td>
        <td width="35%" style="">: <?=$model->jk;?></td>
        <td width="15%" style="">Dusun</td>
        <td width="35%" style="">: <?=$model->dusun;?></td>
    </tr>
    <tr>
        <td width="15%" style="">NIK</td>
        <td width="35%" style="">: <?=$model->nik;?></td>
        <td width="15%" style="">Kelurahan</td>
        <td width="35%" style="">: <?=$model->kelurahan;?></td>
    </tr>
    <tr>
        <td width="15%" style="">NISN</td>
        <td width="35%" style="">: <?=$model->nisn;?></td>
        <td width="15%" style="">Asal Daerah</td>
        <td width="35%" style="">: <?=$model->kecamatan->nama.', '.$model->kecamatan->kota->nama.', '.$model->kecamatan->kota->propinsi->nama;?></td>
    </tr>
    <tr>
        <td width="15%" style="">Nama Ibu Kandung</td>
        <td width="35%" style="">: <?=$model->nama_ibu_kandung;?></td>
        <td width="15%" style="">RT/RW</td>
        <td width="35%" style="">: <?=$model->rt.' / '.$model->rw;?></td>
    </tr>
    <tr>
        <td width="15%" style=""></td>
        <td width="35%" style=""></td>
        <td width="15%" style="">Kodepos</td>
        <td width="35%" style="">: <?=$model->kodepos;?></td>
    </tr>
</table>
<br><br>
<table width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td colspan="4" style="text-align: center;<?=$bottom;?>"><strong>Kontak dan Riwayat Pendidikan</strong></td>
    </tr>
    <tr>
        <td width="15%" style="">Telepon</td>
        <td width="35%" style="">: <?=$model->telepon;?></td>
        <td width="15%" style="">SD</td>
        <td width="35%" style="">: <?=$model->sd;?></td>
    </tr>
    <tr>
        <td width="15%" style="">Email</td>
        <td width="35%" style="">: <?=$model->email;?></td>
        <td width="15%" style="">SMP</td>
        <td width="35%" style="">: <?=$model->smp;?></td>
    </tr>
    <tr>
        <td width="15%" style="">No HP (Whatsapp)</td>
        <td width="35%" style="">: <?=$model->hp;?></td>
        <td width="15%" style="">SMA</td>
        <td width="35%" style="">: <?=$model->sma;?></td>
    </tr>

    <tr>
        <td width="15%" style="">Lulusan Pesantren</td>
        <td width="35%" style="">: <?=$model->is_pesantren == '1' ? 'Ya' : 'Bukan';?></td>
        <td width="15%" style="">Nama Pesantren</td>
        <td width="35%" style="">: <?=$model->nama_pesantren;?></td>
    </tr>
   <tr>
        <td width="15%" style="">Tahun Lulus</td>
        <td width="35%" style="">: <?=$model->tahun_lulus;?></td>
        <td width="15%" style="">Lama Pendidikan</td>
        <td width="35%" style="">: <?=$model->lama_pendidikan;?></td>
    </tr>
    <tr>
        <td width="15%" style="">Takhassus</td>
        <td width="90%" style="" colspan="3">: <?=$model->takhassus;?></td>
    </tr>
</table>
<br><br>
<table width="100%" border="0" cellpadding="4" cellspacing="0" style="padding-bottom: 5px">
    <tr>
        <td colspan="4" style="text-align: center;<?=$bottom;?>"><strong>Data Orang Tua</strong></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;<?=$bottom;?>"><strong>Ayah</strong></td>
        <td colspan="2" style="text-align: center;<?=$bottom;?>"><strong>Ibu</strong></td>
    </tr>
    <tr>
        <td width="15%" style="">Nama</td>
        <td width="35%" >: <?=$model->nama_ayah;?></td>
        <td width="15%" style="">Nama</td>
        <td width="35%" >: <?=$model->nama_ibu;?></td>
    </tr>
    <tr>
        <td width="15%" style="">Tgl Lahir </td>
        <td width="35%" >: <?=$model->tanggal_lahir_ayah;?></td>
        <td width="15%" style="">Tgl Lahir</td>
        <td width="35%" >: <?=$model->tanggal_lahir_ibu;?></td>
    </tr>
    <tr>
        <td width="15%" style="">NIK </td>
        <td width="35%" >: <?=$model->nik_ayah;?></td>
        <td width="15%" style="">NIK</td>
        <td width="35%" >: <?=$model->nik_ibu;?></td>
    </tr>
    <tr>
        <td width="15%" style="">Pendidikan </td>
        <td width="35%" >: <?=$model->pendidikanAyah->nama;?></td>
        <td width="15%" style="">Pendidikan</td>
        <td width="35%" >: <?=$model->pendidikanIbu->nama;?></td>
    </tr>
    <tr>
        <td width="15%" style="">Pekerjaan</td>
        <td width="35%" >: <?=$model->pekerjaanAyah->nama;?></td>
        <td width="15%" style="">Pekerjaan</td>
        <td width="35%" >: <?=$model->pekerjaanIbu->nama;?></td>
    </tr>
    <tr>
        <td width="15%" style="">Penghasilan</td>
        <td width="35%" style="">: <?=$model->penghasilanAyah->nama;?></td>
        <td width="15%" style="">Penghasilan</td>
        <td width="35%" style="">: <?=$model->penghasilanIbu->nama;?></td>
    </tr>
   
</table>
<br><br>
<table width="100%" border="0" cellpadding="4" cellspacing="0" style="padding-bottom: 5px">
    <tr>
        <td colspan="4" style="text-align: center;<?=$bottom;?>"><strong>Informasi Pendaftaran</strong></td>
    </tr>
    <tr>
        <td width="15%" style="">Prodi Pilihan 1</td>
        <td width="85%" style="">: <?=$listProdi[$model->prodi_tujuan1];?></td>
        
    </tr>
    <tr>
        <td width="15%" style="">Prodi Pilihan 2</td>
        <td width="85%" style="">: <?=$listProdi[$model->prodi_tujuan2];?></td>
        
    </tr>
    <tr>
        <td width="15%" style="">Prodi Pilihan 3</td>
        <td width="85%" style="">: <?=$listProdi[$model->prodi_tujuan3];?></td>
        
    </tr>
    <tr>
        <td width="15%" style="">Lulusan KMI Gontor</td>
        <td width="85%" style="">: <?=$model->is_kmi ? 'Ya' : 'Bukan';?></td>
        
    </tr>
    <tr>
        <td width="15%" style="">Rencana Studi di UNIDA Gontor</td>
        <td width="85%" style="">: <?=$model->lama_rencana_kuliah == '1' ? '1 Tahun' : '4 Tahun (S1)';?></td>
        
    </tr>
    <tr>
        <td width="15%" style="">Kampus Tujuan</td>
        <td width="85%" style="">: <?=$listKampus[$model->kampus_id];?></td>
        
    </tr>
</table>


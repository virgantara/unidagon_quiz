
    <table class="table table-hover table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Prodi Pil. 1</th>
          <th>Prodi Pil. 2</th>
          <th>Prodi Pil. 3</th>          
          <?php 
          foreach($subbagian as $sub)
          {
            echo '<th>'.$sub->nama.'</th>';
          }
          ?>
          <th>Total Skor</th>
        </tr>
      </thead>
      <tbody>
        <?php 

        foreach($pesertas as $q => $m)
        {

        ?>
        <tr>
          <td><?=$q+1;?></td>
          <td><?=$m['nama'];?></td>
          <td><?=$m['p1'];?></td>
          <td><?=$m['p2'];?></td>
          <td><?=$m['p3'];?></td>
          <?php 
          $total = 0;
          foreach($subbagian as $sub)
          {
            $sum = $results[$m['id']][$sub->id];
            $total += $sum;
            echo '<td>'.round($sum,2).'</td>';
          }
          ?>

          <td><?=round($total,2);?></td>
        </tr>
        <?php 
        }
        ?>

      
      </tbody>
    </table>

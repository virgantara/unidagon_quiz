<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PesertaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="peserta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'tempat_lahir') ?>

    <?= $form->field($model, 'tanggal_lahir') ?>

    <?= $form->field($model, 'jk') ?>

    <?php // echo $form->field($model, 'agama_id') ?>

    <?php // echo $form->field($model, 'status_warga_negara') ?>

    <?php // echo $form->field($model, 'nik') ?>

    <?php // echo $form->field($model, 'nisn') ?>

    <?php // echo $form->field($model, 'nama_ibu_kandung') ?>

    <?php // echo $form->field($model, 'jalan') ?>

    <?php // echo $form->field($model, 'dusun') ?>

    <?php // echo $form->field($model, 'kelurahan') ?>

    <?php // echo $form->field($model, 'kecamatan_id') ?>

    <?php // echo $form->field($model, 'rt') ?>

    <?php // echo $form->field($model, 'rw') ?>

    <?php // echo $form->field($model, 'kodepos') ?>

    <?php // echo $form->field($model, 'jenis_tinggal_id') ?>

    <?php // echo $form->field($model, 'alat_transportasi_id') ?>

    <?php // echo $form->field($model, 'telepon') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'hp') ?>

    <?php // echo $form->field($model, 'penerima_kps') ?>

    <?php // echo $form->field($model, 'no_kps') ?>

    <?php // echo $form->field($model, 'is_pesantren') ?>

    <?php // echo $form->field($model, 'nama_pesantren') ?>

    <?php // echo $form->field($model, 'tahun_lulus') ?>

    <?php // echo $form->field($model, 'lama_pendidikan') ?>

    <?php // echo $form->field($model, 'takhassus') ?>

    <?php // echo $form->field($model, 'sd') ?>

    <?php // echo $form->field($model, 'smp') ?>

    <?php // echo $form->field($model, 'sma') ?>

    <?php // echo $form->field($model, 'nama_ayah') ?>

    <?php // echo $form->field($model, 'nik_ayah') ?>

    <?php // echo $form->field($model, 'tanggal_lahir_ayah') ?>

    <?php // echo $form->field($model, 'pendidikan_ayah_id') ?>

    <?php // echo $form->field($model, 'pekerjaan_ayah_id') ?>

    <?php // echo $form->field($model, 'penghasilan_ayah_id') ?>

    <?php // echo $form->field($model, 'nama_ibu') ?>

    <?php // echo $form->field($model, 'nik_ibu') ?>

    <?php // echo $form->field($model, 'tanggal_lahir_ibu') ?>

    <?php // echo $form->field($model, 'pendidikan_ibu_id') ?>

    <?php // echo $form->field($model, 'pekerjaan_ibu_id') ?>

    <?php // echo $form->field($model, 'penghasilan_ibu_id') ?>

    <?php // echo $form->field($model, 'nama_wali') ?>

    <?php // echo $form->field($model, 'tanggal_lahir_wali') ?>

    <?php // echo $form->field($model, 'pendidikan_wali_id') ?>

    <?php // echo $form->field($model, 'pekerjaan_wali_id') ?>

    <?php // echo $form->field($model, 'penghasilan_wali_id') ?>

    <?php // echo $form->field($model, 'prodi_tujuan1') ?>

    <?php // echo $form->field($model, 'prodi_tujuan2') ?>

    <?php // echo $form->field($model, 'prodi_tujuan3') ?>

    <?php // echo $form->field($model, 'is_kmi') ?>

    <?php // echo $form->field($model, 'lama_rencana_kuliah') ?>

    <?php // echo $form->field($model, 'kampus_id') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

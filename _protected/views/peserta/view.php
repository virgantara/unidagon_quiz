<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Peserta */

$this->title = 'Biodata '.$model->nama ?: Yii::$app->name;
// $this->params['breadcrumbs'][] = ['label' => 'Pesertas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-xs-12">
        <p>
        <?php
$periode = \app\models\Periode::find()->where(['status_aktivasi'=>'Y'])->one();
if(!empty($periode) && \app\helpers\MyHelper::isBetween($periode->tanggal_buka,$periode->tanggal_tutup))
{

    echo Html::a('<i class="fa fa-edit"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-success']);
    echo '&nbsp;';
}

echo Html::a('<i class="fa fa-print"></i> Print', ['print', 'id' => $model->id], ['class' => 'btn btn-primary','target'=>'_blank']);
        // echo Html::a('Delete', ['delete', 'id' => $model->id], [
        //     'class' => 'btn btn-danger',
        //     'data' => [
        //         'confirm' => 'Are you sure you want to delete this item?',
        //         'method' => 'post',
        //     ],
        // ]);
         ?>
     </p>
    </div>

    
<div class="col-lg-6">

<div class="panel panel-primary">
    <div class="panel-heading">Data Calon Mahasiswa</div>
    <div class="panel-body">
        <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name"> Nama </div>

                <div class="profile-info-value">
                    <span class="editable" id="username"> <?= $model->nama;?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> TTL  </div>

                <div class="profile-info-value">
                    <span class="editable" id="username">  <?= $model->tempat_lahir;?>, <?= $model->tanggal_lahir;?></span>
                </div>
            </div>            
             <div class="profile-info-row">
                <div class="profile-info-name"> Jenis Kelamin  </div>

                <div class="profile-info-value">
                    <span class="editable" id="username">  <?= $model->jk;?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> NIK </div>

                <div class="profile-info-value">
                    <span class="editable">  <?= $model->nik;?></span>
                </div>
            </div>   
             <div class="profile-info-row">
                <div class="profile-info-name"> NISN </div>

                <div class="profile-info-value">
                    <span class="editable">  <?= $model->nisn;?></span>
                </div>
            </div>  
            <div class="profile-info-row">
                <div class="profile-info-name"> Nama Ibu Kandung </div>

                <div class="profile-info-value">
                    <span class="editable">  <?= $model->nama_ibu_kandung;?></span>
                </div>
            </div>  
             <div class="profile-info-row">
                <div class="profile-info-name"> Agama </div>

                <div class="profile-info-value">
                    <span class="editable">  <?= $model->agama_id;?></span>
                </div>
            </div>    
        </div>

       
        </div>
        
               
    </div>
    </div>
    <div class="col-lg-6">   
<div class="panel panel-primary">
    <div class="panel-heading">Alamat</div>
    <div class="panel-body">
        <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name"> Status warga negara </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->status_warga_negara;?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> Jalan </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->jalan;?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> Dusun </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->dusun;?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> Kelurahan </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->kelurahan;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> Kecamatan, Kabupaten, Provinsi </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->kecamatan->nama.', '.$model->kecamatan->kota->nama.', '.$model->kecamatan->kota->propinsi->nama;?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> RT/RW </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->rt.' / '.$model->rw;?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> Kodepos </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->kodepos;?></span>
                </div>
            </div>
        </div>
                
                
                
    </div>
</div>
</div>
<div class="col-lg-6">   
<div class="panel panel-primary">
    <div class="panel-heading">Kontak</div>
    <div class="panel-body">
        <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name"> Telepon </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->telepon;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> Email </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->email;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> No HP </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->hp;?></span>
                </div>
            </div>
          
        </div>
                
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">Riwayat Pendidikan</div>
    <div class="panel-body">
        <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name"> SD </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->sd;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> SMP </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->smp;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> SMA </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->sma;?></span>
                </div>
            </div>
        </div>
              
    </div>
</div>
</div>
<div class="col-lg-6">   
     <div class="panel panel-primary">
    <div class="panel-heading">Lainnya</div>
    <div class="panel-body">           
        <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name"> Lulusan Pesantren </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->is_pesantren=='1' ? 'Ya' : 'Bukan';?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> Nama pesantren </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->nama_pesantren;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> Tahun lulus </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->tahun_lulus;?></span>
                </div>
            </div>
          <div class="profile-info-row">
                <div class="profile-info-name"> Lama pendidikan </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->lama_pendidikan;?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> Takhassus </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->takhassus;?></span>
                </div>
            </div>
        </div>
        
    </div>
</div>
</div>
</div>
<div class="row">
<div class="row">
<div class="col-lg-6">   
    <div class="panel panel-primary">
    <div class="panel-heading">Data Ayah</div>
    <div class="panel-body">
        <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name"> Nama Ayah </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->nama_ayah;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> Tgl Lahir </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->tanggal_lahir_ayah;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> NIK </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->nik_ayah;?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> Pendidikan </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->pendidikanAyah->nama;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> Pekerjaan </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->pekerjaanAyah->nama;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> Penghasilan </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->penghasilanAyah->nama;?></span>
                </div>
            </div>
        </div>
            
              
    </div></div>
</div>
<div class="col-lg-6">   
        <div class="panel panel-primary">
    <div class="panel-heading">Data ibu</div>
    <div class="panel-body">
              <div class="profile-user-info profile-user-info-striped">
                <div class="profile-info-row">
                <div class="profile-info-name"> Nama Ibu </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->nama_ibu;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> Tgl Lahir </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->tanggal_lahir_ibu;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> NIK </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->nik_ibu;?></span>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name"> Pendidikan </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->pendidikanIbu->nama;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> Pekerjaan </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->pekerjaanIbu->nama;?></span>
                </div>
            </div>
             <div class="profile-info-row">
                <div class="profile-info-name"> Penghasilan </div>

                <div class="profile-info-value">
                    <span class="editable"> <?= $model->penghasilanIbu->nama;?></span>
                </div>
            </div>
        </div>
    </div></div>
    </div>
</div>
    <div class="col-lg-6">   
        <div class="panel panel-primary">
            <div class="panel-heading">Informasi </div>
            <div class="panel-body">
                <div class="profile-user-info profile-user-info-striped">
                    <div class="profile-info-row">
                        <div class="profile-info-name"> Prodi tujuan 1 </div>

                        <div class="profile-info-value">
                            <span class="editable"> <?= $listProdi[$model->prodi_tujuan1];?></span>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name"> Prodi tujuan 2 </div>

                        <div class="profile-info-value">
                            <span class="editable"> <?= $listProdi[$model->prodi_tujuan2];?></span>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name"> Prodi tujuan 3 </div>

                        <div class="profile-info-value">
                            <span class="editable"> <?= $listProdi[$model->prodi_tujuan3];?></span>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name"> Lulusan KMI Gontor</div>

                        <div class="profile-info-value">
                            <span class="editable"> <?= $model->is_kmi == '1' ? 'Ya' : 'Bukan';?></span>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name"> Rencana Studi di UNIDA Gontor </div>

                        <div class="profile-info-value">
                            <span class="editable"> <?= $model->lama_rencana_kuliah;?></span>
                        </div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-name"> Kampus mengikuti perkuliahan </div>

                        <div class="profile-info-value">
                            <span class="editable"> <?= $listKampus[$model->kampus_id];?></span>
                        </div>
                    </div>
                </div>
                       
               </div>
           </div>
               

    </div>

    <div class="col-lg-6">   
        <div class="panel panel-primary">
            <div class="panel-heading">Bukti Pendaftaran </div>
            <div class="panel-body">
                <div class="profile-user-info profile-user-info-striped">
                    <?php 
                    foreach($model->pesertaSyarats as $s)
                    {
                    ?>
                    <div class="profile-info-row">
                        <div class="profile-info-name"> <?=$s->bukti->nama;?> </div>

                        <div class="profile-info-value">
                            <span class="editable"> <a target="_blank" href="<?= Url::base(true).'/'.$s->file_path;?>">Lokasi File</a></span>
                        </div>
                    </div>
                    <?php }?>
                </div>
                       
               </div>
           </div>
               

    </div>
</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PesertaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Peserta';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peserta-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
$gridColumns = [
    [
        'class'=>'kartik\grid\SerialColumn',
        'contentOptions'=>['class'=>'kartik-sheet-style'],
        'width'=>'36px',
        'pageSummary'=>'Total',
        'pageSummaryOptions' => ['colspan' => 6],
        'header'=>'',
        'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    'nama',
    'tempat_lahir',
    'tanggal_lahir',
    'jk',
    'token',
    // 'nik',
    // 'nama_ibu_kandung',
    // 'email:email',
    // 'hp',
    
    
    [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view} {update} {delete}',
        'visibleButtons' => [
            'update' => function ($model) {
                return \Yii::$app->user->can('member');
            },
            'delete' => function ($model) {
                return \Yii::$app->user->can('admin');
            },
        ]
    ],
]
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'], 
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'containerOptions' => ['style'=>'overflow: auto'], 
        'beforeHeader'=>[
            [
                'columns'=>[
                    ['content'=> $this->title, 'options'=>['colspan'=>14, 'class'=>'text-center warning']], //cuma satu kolom header
            //        ['content'=>'', 'options'=>['colspan'=>0, 'class'=>'text-center warning']], //uncomment kalau mau membuat header kolom-2
              //      ['content'=>'', 'options'=>['colspan'=>0, 'class'=>'text-center warning']],
                ], //uncomment kalau mau membuat header kolom-3
                'options'=>['class'=>'skip-export'] 
            ]
        ],
        'exportConfig' => [
              // GridView::PDF => ['label' => 'Save as PDF'],
              GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
              // GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
              GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
          ],
          
        'toolbar' =>  [
            '{export}', 

           '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        'pjax' => true,
        'pjaxSettings' =>[
            'neverTimeout'=>true,
            'options'=>[
                'id'=>'pjax_id',
            ]
        ],  
        'bordered' => true,
        'striped' => true,
        // 'condensed' => false,
        // 'responsive' => false,
        'hover' => true,
        // 'floatHeader' => true,
        // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
        'panel' => [
            'type' => GridView::TYPE_PRIMARY
        ],
    ]); ?>
</div>


<?php

$this->registerJs(' 

$(document).on("click",".btn-terima",function(){
    Swal.fire({
      title: \'Terima mahasiswa ini?\',
      
      icon: \'warning\',
      showCancelButton: true,
      confirmButtonColor: \'#3085d6\',
      cancelButtonColor: \'#d33\',
      confirmButtonText: \'Ya, terima!\'
    }).then((result) => {
      if (result.value) {
        var id = $(this).attr("data-item");
        var obj = new Object;
        obj.id = id;
        obj.kode = $(this).attr("data-kode");
        $.ajax({

            type : "POST",
            url : "'.Url::to(['/peserta/approval']).'",
            data : {
                dataku : obj
            },
            error: function(e){
                Swal.hideLoading();
            },
            beforeSend: function(){
               Swal.fire({
                  title: \'Sedang diproses!\',
                  html: \'\',
                  
                  onBeforeOpen: () => {
                    Swal.showLoading()
                    
                  },
                  
                }).then((result) => {
                  
                  
                })
            },
            success: function(data){
                Swal.hideLoading();
                var hasil = $.parseJSON(data);

                Swal.fire(
                \'Good job!\',
                  hasil.msg,
                  \'success\'
                );
                $.pjax.reload({container:\'#pjax_id\'});
            }
        });
      }
    })
    
});


$(document).on("click",".btn-tolak",function(){
    Swal.fire({
      title: \'Tolak mahasiswa ini?\',
      
      icon: \'warning\',
      showCancelButton: true,
      confirmButtonColor: \'#3085d6\',
      cancelButtonColor: \'#d33\',
      confirmButtonText: \'Ya, tolak!\'
    }).then((result) => {
      if (result.value) {
        var id = $(this).attr("data-item");
        var obj = new Object;
        obj.id = id;
        obj.kode = $(this).attr("data-kode");
        $.ajax({

            type : "POST",
            url : "'.Url::to(['/peserta/approval']).'",
            data : {
                dataku : obj
            },
            error: function(e){
                Swal.hideLoading();
            },
            beforeSend: function(){
                Swal.fire({
                  title: \'Auto close alert!\',
                  html: \'I will close in <b></b> milliseconds.\',
                  timer: 2000,
                  timerProgressBar: true,
                  onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                      const content = Swal.getContent()
                      if (content) {
                        const b = content.querySelector(\'b\')
                        if (b) {
                          b.textContent = Swal.getTimerLeft()
                        }
                      }
                    }, 100)
                  },
                  onClose: () => {
                    clearInterval(timerInterval)
                  }
                }).then((result) => {
                  /* Read more about handling dismissals below */
                  if (result.dismiss === Swal.DismissReason.timer) {
                    console.log(\'I was closed by the timer\')
                  }
                })
            },
            success: function(data){
                var hasil = $.parseJSON(data);

                Swal.fire(
                \'Good job!\',
                  hasil.msg,
                  \'success\'
                );
                $.pjax.reload({container:\'#pjax_id\'});
            }
        });
      }
    })
    
});

    ', \yii\web\View::POS_READY);

?>

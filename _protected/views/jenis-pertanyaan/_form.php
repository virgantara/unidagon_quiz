<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JenisPertanyaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jenis-pertanyaan-form">

    <?php $form = ActiveForm::begin([
    	'options' => [
    		'class' => 'form-horizontal'
    	]
    ]); 

    echo $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']);
    
    ?>
    <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Kode Jenis</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'id',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Nama</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'nama',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
            <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JenisPertanyaan */

$this->title = 'Create Jenis Pertanyaan';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Pertanyaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-pertanyaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

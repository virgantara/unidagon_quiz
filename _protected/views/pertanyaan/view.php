<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

\app\assets\SoalAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Pertanyaan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pertanyaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<script id="MathJax-script" async
  src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js">
</script>
<div class="pertanyaan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
 
     <?php

// \app\assets\MathJaxAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Pertanyaan */
/* @var $form yii\widgets\ActiveForm */

$is_image_exist = !empty($model->foto_path);
?>
<style type="text/css">
     /* Customize the label (the check-container) */

.green_back{
    background-color: green;
    color:white;
}
</style>
<div class="pertanyaan-form">

     <?php    
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
     ?>
        <div class="alert alert-<?=$key?>"><?=$message?></div>
     <?php     }
     ?>    

         <?php 
        if($is_image_exist)
        {
        ?>
        <div class="row">
            <div class="col-xs-12 text-center">
                <p><img src="<?=Url::base(true).'/'.$model->foto_path;?>" width="100%"></p>
            </div>
        </div>
        <?php
        }
        ?>
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <h1><?=$model->nama;?></h1>
            </div>
        </div>
        

        <div class="row">
            <div class="col-xs-4 col-xs-offset-1">
                <label class="check-container"><?=$model->jawaban1;?></h3>
                    <input type="radio" name="Pertanyaan[jawaban_benar]" value="1"> 
                    <span class="checkmark"></span>
                </label>
                
            </div>
            <div class="col-xs-4 col-xs-offset-1">
                <label class="check-container"><?=$model->jawaban2;?></h3>
                    <input type="radio" name="Pertanyaan[jawaban_benar]" value="2"> 
                    <span class="checkmark"></span>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-xs-offset-1">
        
                <label class="check-container"><?=$model->jawaban3;?></h3>
                    <input type="radio" name="Pertanyaan[jawaban_benar]" value="3"> 
                    <span class="checkmark"></span>
                </label>
            </div>
            <div class="col-xs-4 col-xs-offset-1">
                <label class="check-container"><?=$model->jawaban4;?></h3>
                    <input type="radio" name="Pertanyaan[jawaban_benar]" value="4"> 
                    <span class="checkmark"></span>
                </label>
            </div>
        </div>
            
            

</div>

</div>

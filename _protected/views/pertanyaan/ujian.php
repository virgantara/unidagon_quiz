<?php


use app\models\Jawaban;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

\app\assets\SoalAsset::register($this);
\app\assets\CountDownAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Pertanyaan */

$this->title = 'Ujian Online PMB UNIDA Gontor';
?>
<style type="text/css">
    .swal-wide{
    width:850px !important;
}
</style>
<div class="row">

    <h1 class=" text-center"><?= Html::encode($this->title) ?></h1>
<div class="alert alert-info text-center"><h1><i class="fa fa-warning"></i><span id="teks_info"></span></h1> <span id="hitung" style="font-size: 90px"></span></div>
<?php 
    $sd = $quiz->time_start;
    $ed = $quiz->time_end;

    $tglnow = date('Y-m-d H:i:s');

    if($tglnow >= $sd && $tglnow <= $ed)
    {



?>

   
    <div class="row">
        <div class="col-xs-12">
            <!-- PAGE CONTENT BEGINS -->
            <div class="row">
                <?php
     foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
         echo '<div class="text-center alert alert-' . $key . '">' . $message . '</div>';
     } ?>
                <?php 

               
                foreach($results as $item)
                {
                ?>
                <div class="col-xs-6 col-sm-3 pricing-box">
                    <div class="widget-box widget-color-blue">
                        <div class="widget-header">
                            <h2 class="widget-title bigger"><?=$item['nama'];?></h2>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <ul class="list-unstyled spaced2">
                                    
                                    <?php 
                                    $total  = 0;
                                    
                                    foreach($item['items'] as $sub)
                                    {
                                        // $total_terjawab =0;
                                        $total += $sub['jml'];
                                        // foreach($sub->pertanyaans as $per)
                                        //     $total_terjawab += count($per->jawabans);
                                    ?>
                                    <li>
                                        <i class="ace-icon fa fa-check green"></i>
                                        <?=$sub['nama'];?> (<?=$sub['jml'];?> soal)
                                        
                                    </li>
                                    <?php 
                                    }
                                    ?>
                                    
                                </ul>

                                <hr />
                                <div class="price">
                                    Total <?=$total;?>
                                    <small>soal</small>
                                </div>
                            </div>

                            <div>
                                <a href="<?=Url::to(['pertanyaan/soal','id'=>$item['jpid']]);?>" class="btn btn-block btn-primary">
                                    <i class="ace-icon glyphicon glyphicon-play-circle  bigger-110"></i>
                                    <span>Mulai Sekarang</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                
            }
                ?>
               

                

        
            </div>

            
        </div><!-- /.col -->
    </div><!-- /.row -->
    <?php 
    }
    ?>
</div>

<hr>
<div class="text-center">
        <?=Html::a('<i class="glyphicon glyphicon-remove"></i> Tutup Ujian',['pertanyaan/selesai'],['class'=>'btn btn-danger btn-white btn-bold btn-round','id'=>'btn-tutup']);?>
    
</div>


<?php
$script = '
    $("#btn-tutup").click(function(e){
        e.preventDefault();
        Swal.fire({
          customClass: \'swal-wide\',
          title: \'Apakah Anda ingin mengakhiri ujian ini?\',
          text : \'Anda masih bisa masuk kembali jika sisa waktu masih ada\',
          icon: \'warning\',
          showCancelButton: true,
          confirmButtonColor: \'#3085d6\',
          cancelButtonColor: \'#d33\',
          confirmButtonText: \'Ya!\'
        }).then((result) => {
          if (result.value) {
            window.location.href = $(this).attr("href");
          }
        });
    });
    
    

';
if($tglnow < $sd)
{
$script .= '

    $("#teks_info").html(" Jadwal ujian online akan dimulai dalam ");
    $(\'#hitung\').countdown(\''.$sd.'\', function(event) {
        $(this).html(event.strftime(\'%d hari %H jam %M menit %S detik\'));
    }).on(\'finish.countdown\',function(){
        window.location.href = window.location.href;
    });
';
}

else if($tglnow >= $sd && $tglnow <= $ed)
{
$script .= '
    $("#teks_info").html(" Sisa waktu: ");
    $(\'#hitung\').countdown(\''.$ed.'\', function(event) {
        $(this).html(event.strftime(\'%H jam %M menit %S detik\'));
    }).on(\'finish.countdown\',function(){
        window.location.href = window.location.href;
    });
';

}

else if($tglnow > $ed)
{
$script .= '
    $("#teks_info").html(" Ujian Telah Selesai ");
    
';

}

$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);


?>

<?php


use app\models\Jawaban;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

\app\assets\SoalAsset::register($this);

\app\assets\CountDownAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Pertanyaan */

$this->title = 'Ujian Online PMB UNIDA Gontor';
?> 
<style type="text/css">
@font-face {font-family: "Traditional Arabic"; src: url("//db.onlinewebfonts.com/t/967bcfce977ad86229fdc54d85520dcd.eot"); src: url("//db.onlinewebfonts.com/t/967bcfce977ad86229fdc54d85520dcd.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/967bcfce977ad86229fdc54d85520dcd.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/967bcfce977ad86229fdc54d85520dcd.woff") format("woff"), url("//db.onlinewebfonts.com/t/967bcfce977ad86229fdc54d85520dcd.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/967bcfce977ad86229fdc54d85520dcd.svg#Traditional Arabic") format("svg"); } 

.fontarab{
    font-family: "Traditional Arabic";
    font-size: 30px
}

    /*.odd{
        background-color: #FBF0D9;
    }

    .even{
        background-color: #FFE6BA;
    }*/
    .bg_sub{
        background-color: #313030;
        color:white;
       
        border-radius: 10px;
    }
</style>
<script id="MathJax-script" async
  src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js">
</script>
<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'form-horizontal
'    ]
]); ?>
<div class="pertanyaan-view">
    <span>Sisa waktu: <span id="hitung" style="background-color: aqua;padding:5px"></span>
    <h1 class=" text-center "><?=$jenisPertanyaan->nama;?></h1>
    </span>
<?php 


$arr = [];
foreach($results as $q => $res)
{
    
    ?>
    <div class="row">
        <div class="col-xs-12 bg_sub" ><h2 class=" text-center">-- <?=$res['nama'];?> --</h2></div>
    </div>
    <?php

    

   
    
    foreach($res['items'] as $model)
    {
        $model = (object)$model;
        $is_image_exist = !empty($model->foto_path);
        
        $jawaban = $listJawabans[$peserta->id][$model->id];
    ?>
    <div class="row" style="padding-bottom: 5px;">
        <div class="col-xs-1 text-right">
            <h3><?=$model->nomor_soal;?></h3> 
        </div>
        <div class="col-xs-10 <?=($q+1) % 2 == 0 ? 'odd' : 'even';?>">
     
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="fontarab"><?=$model->nama;?></h3>
                </div>
                
            </div>

            <?php 
                if($is_image_exist)
                {
                ?>
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p><img src="<?=Url::base(true).'/'.$model->foto_path;?>"></p>
                    </div>
                </div>
                <?php
                }
                ?>

            <div class="row" style="padding-bottom: 5px">
                <div class="col-xs-5" >
                    <label class="check-container" style="border: solid 1px gray;border-radius: 0.5em"><h3 class="fontarab"><?=$model->jawaban1;?></h3>
                        <input type="radio" name="ans<?=$model->id;?>" value="1" <?=$jawaban == 1 ? 'checked' : '';?> > 
                        <span class="checkmark"></span>
                    </label>
                    
                </div>
                <div class="col-xs-2"></div>
                <div class="col-xs-5" >
                    <label class="check-container" style="border: solid 1px gray;border-radius: 0.5em"><h3 class="fontarab"><?=$model->jawaban2;?></h3>
                        <input type="radio" name="ans<?=$model->id;?>" value="2" <?=$jawaban == 2 ? 'checked' : '';?> > 
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5" >
            
                    <label class="check-container" style="border: solid 1px gray;border-radius: 0.5em"><h3 class="fontarab"><?=$model->jawaban3;?></h3>
                        <input type="radio" name="ans<?=$model->id;?>" value="3" <?=$jawaban == 3 ? 'checked' : '';?> > 
                        <span class="checkmark"></span>
                    </label>
                </div><div class="col-xs-2"></div>
                <div class="col-xs-5" >
                    <label class="check-container" style="border: solid 1px gray;border-radius: 0.5em"><h3 class="fontarab"><?=$model->jawaban4;?></h3>
                        <input type="radio" name="ans<?=$model->id;?>" value="4" <?=$jawaban == 4 ? 'checked' : '';?> > 
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
                
                
        </div>

    </div>
    <?php 
    }
}
?>
    <div class="col-xs-offset-1 col-xs-10" style="padding-top: 50px">
        <?= Html::submitButton('Simpan Hasil', ['class' => 'btn btn-primary btn-lg btn-block','value'=>1,'name'=>'btn-simpan']) ?>
    </div>
</div>
<?php ActiveForm::end();?>



<?php
$script = '

';

$sd = $quiz->time_start;
$ed = $quiz->time_end;

$tglnow = date('Y-m-d H:i:s');
if($tglnow >= $sd && $tglnow <= $ed)
{
$script .= '
    $(\'#hitung\').countdown(\''.$ed.'\', function(event) {
        $(this).html(event.strftime(\'%H jam %M menit %S detik\'));
    }).on(\'finish.countdown\',function(){
        window.location.href = window.location.href;
    });
';

}

$this->registerJs(
    $script,
    \yii\web\View::POS_READY
);


?>

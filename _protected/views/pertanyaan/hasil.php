<div class="row">
	<div class="col-xs-6 col-xs-offset-3">
		<table class="table table-hover table-striped">
			<thead>
			<tr>
				
				
				<th class="text-center">Bidang</th>
				<th>Materi</th>
			</tr>
			</thead>
			<tbody>
				<?php 
				$i = 0;
				foreach($hasil as  $p)
		        {
		        	?>
		        	<tr>
		        		<td colspan="2" class="text-center"><strong><?=$p->jenisPertanyaan->nama;?></strong></td>
		        	</tr>
		        	<?php
		        	$subtotal = 0;
		            foreach($p->jenisPertanyaan->subbagians as $sub)
		            {

		            	$i++;
		                $total = $results[$p->id][$sub->id];
		           		$subtotal += $total;
				?>
				<tr>
					
					<td  class="text-center"><?=$sub->nama;?></td>
					<td><?=$total;?></td>
				</tr>
				<?php 
				 	}

				 	?>
				 	<tr>
						<td class="text-right">Total skor <?=$p->jenisPertanyaan->nama;?></td>
						
						<td><strong><?=$subtotal;?></strong></td>
					</tr>
				 	<?php
		        }
				?>
			</tbody>
		</table>
	</div>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PertanyaanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pertanyaan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'jenis_pertanyaan_id') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'jawaban1') ?>

    <?= $form->field($model, 'jawaban2') ?>

    <?php // echo $form->field($model, 'jawaban3') ?>

    <?php // echo $form->field($model, 'jawaban4') ?>

    <?php // echo $form->field($model, 'jawaban_benar') ?>

    <?php // echo $form->field($model, 'poin') ?>

    <?php // echo $form->field($model, 'foto_path') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

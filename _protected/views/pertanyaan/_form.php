<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
// use dosamigos\ckeditor\CKEditor;
use mihaildev\ckeditor\CKEditor;
use app\models\JenisPertanyaan;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;


\app\assets\SoalAsset::register($this);
// \app\assets\MathJaxAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Pertanyaan */
/* @var $form yii\widgets\ActiveForm */

$jenis_id = '';
if(!$model->isNewRecord)
{
  $jenis_id = $model->subbagian->jenis_id;
}
?>
<script id="MathJax-script" async
  src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js">
</script>
<div class="pertanyaan-form">

    <?php $form = ActiveForm::begin([
    	'options' => [
    		'class' => 'form-horizontal',
            'enctype' => 'multipart/form-data'
    	]
    ]); 

    echo $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']);
    
    ?>
     <?php    
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
     ?>
        <div class="alert alert-<?=$key?>"><?=$message?></div>
     <?php     }
     ?>    
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <div class="form-group">
                    <label>Jenis pertanyaan</label>
                    <div>
            <?= Select2::widget([
                'name' => 'jenis_pertanyaan',
                'value' => $jenis_id,
                'data' => ArrayHelper::map(JenisPertanyaan::find()->all(),'id','nama'),

                'options'=>['id'=>'jenis_pertanyaan','placeholder'=>Yii::t('app','- Pilih Jenis Pertanyaan -')],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>

                    </div>
                </div>
                <div class="form-group">
                    <label>Sub Bagian</label>
                    <div>
                   <?= $form->field($model, 'subbagian_id',['options' => ['tag' => false]])->widget(DepDrop::classname(), [
                    'type'=>DepDrop::TYPE_SELECT2,
                    'options'=>['id'=>'subbagian_id'],
                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                    'pluginOptions'=>[
                        'depends'=>['jenis_pertanyaan'],
                        'initialize' => true,
                        'placeholder'=>'- Pilih Subbagian -',
                        'url'=>Url::to(['/subbagian/sub'])
                    ]
                ])->label(false) ?>

                    </div>
                </div>
                <div class="form-group">
                    <label>Nomor Soal</label>
                    <div>
                    <?= $form->field($model, 'nomor_soal',['options' => ['tag' => false]])->textInput(['class'=>'form-control'])->label(false) ?>

                    </div>
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <div>
                    <?= $form->field($model, 'nama')->widget(CKEditor::className(), [
                        // 'options' => ['rows' => 6],
                        'editorOptions' => [
                            'preset' => 'full',
                            'inline' => false,
                            'enterMode' => new \yii\web\JsExpression('CKEDITOR.ENTER_BR'),
                        ],

                        // 'clientOptions' => [
                        //     'enterMode' => new \yii\web\JsExpression('CKEDITOR.ENTER_BR'),
                        // ]
                    ])->label(false) ?>

                    <span id="preview"><?=$model->nama;?></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6">
        
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Jawaban 1</label>
                    <div class="col-sm-6">
                       
                    <?= $form->field($model, 'jawaban1',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>
                    
                    </div>
                    <div class="col-sm-3">
                         <label class="check-container">Set Benar
                            <input type="radio" name="Pertanyaan[jawaban_benar]" value="1" <?=$model->jawaban_benar==1 ? "checked" :"";?>> 
                            <span class="checkmark"></span>
                        </label>
                     </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Jawaban 2</label>
                    <div class="col-sm-6">
                    <?= $form->field($model, 'jawaban2',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

                    </div>
                     <div class="col-sm-3">
                        <label class="check-container">Set Benar
                            <input type="radio" name="Pertanyaan[jawaban_benar]" value="2" <?=$model->jawaban_benar==2 ? "checked" :"";?>> 
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
        
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Jawaban 3</label>
                    <div class="col-sm-6">
                    <?= $form->field($model, 'jawaban3',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

                    </div>
                     <div class="col-sm-3">
                         <label class="check-container">Set Benar
                            <input type="radio" name="Pertanyaan[jawaban_benar]" value="3" <?=$model->jawaban_benar==3 ? "checked" :"";?>> 
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label class="col-sm-3 control-label no-padding-right">Jawaban 4</label>
                    <div class="col-sm-6">
                    <?= $form->field($model, 'jawaban4',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

                    </div>
                     <div class="col-sm-3">
                         <label class="check-container">Set Benar
                            <input type="radio" name="Pertanyaan[jawaban_benar]" value="4" <?=$model->jawaban_benar==4 ? "checked" :"";?>> 
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Poin</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'poin',['options' => ['tag' => false]])->textInput()->label(false) ?>

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Upload Gambar jika ada</label>
            <div class="col-sm-6">
            <?= $form->field($model, 'buktiFile',['options' => ['tag' => false]])->fileInput()->label(false) ?>

         <?php 
        if(!empty($model->foto_path))
        {
        ?>
        <div class="row">
            <div class="col-xs-12 text-center">
                <p><img src="<?=Url::base(true).'/'.$model->foto_path;?>" width="100%"></p>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label no-padding-right">Hapus Gambar?</label>
            <div class="">
            <?= $form->field($model, 'deleteFile',['options' => ['tag' => false]])->checkBox()->label(false) ?>

            </div>
        </div>
        <?php
        }
        ?>
            </div>
        </div>
            <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 


$this->registerJs(' 

// $(document).on(\'keyup\',\'#pertanyaan-nama\',function(){

//     $(\'#preview\').html($(this).val());
// });  

  ', \yii\web\View::POS_READY);

?>
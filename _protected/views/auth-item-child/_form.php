<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$listItem = \yii\helpers\ArrayHelper::map(\app\models\AuthItem::find()->all(),'name','name');
/* @var $this yii\web\View */
/* @var $model app\models\AuthItemChild */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-child-form">

    <?php $form = ActiveForm::begin([
    	'options' => [
    		'class' => 'form-horizontal'
    	]
    ]); ?>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Parent</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'parent',['options' => ['tag' => false]])->dropDownList($listItem,['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
                <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Child</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'child',['options' => ['tag' => false]])->dropDownList($listItem,['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
            <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

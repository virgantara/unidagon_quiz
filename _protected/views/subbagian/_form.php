<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\JenisPertanyaan;
/* @var $this yii\web\View */
/* @var $model app\models\Subbagian */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subbagian-form">

    <?php $form = ActiveForm::begin([
    	'options' => [
    		'class' => 'form-horizontal'
    	]
    ]); 

    echo $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']);
    
    ?>
     <?php    
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
     ?>
        <div class="alert alert-<?=$key?>"><?=$message?></div>
     <?php     }
     ?>    <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Jenis</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'jenis_id',['options' => ['tag' => false]])->dropDownList(ArrayHelper::map(JenisPertanyaan::find()->all(),'id','nama'),['class'=>'form-control','maxlength' => true,'prompt'=>'- Pilih Jenis Pertanyaan-'])->label(false) ?>

            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Nama</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'nama',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
            <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

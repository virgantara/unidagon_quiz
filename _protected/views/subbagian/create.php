<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Subbagian */

$this->title = 'Create Subbagian';
$this->params['breadcrumbs'][] = ['label' => 'Subbagians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subbagian-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

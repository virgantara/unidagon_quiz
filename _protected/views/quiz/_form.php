<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Quiz */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="quiz-form">

    <?php $form = ActiveForm::begin([
    	'options' => [
    		'class' => 'form-horizontal'
    	]
    ]); 

    echo $form->errorSummary($model,['header'=>'<div class="alert alert-danger">','footer'=>'</div>']);
    
    ?>
     <?php    
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
     ?>
        <div class="alert alert-<?=$key?>"><?=$message?></div>
     <?php     }
     ?>    <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Nama</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'nama',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true])->label(false) ?>

            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Tanggal</label>
            <div class="col-sm-9"><i class="fa fa-calendar"></i>
            <?= $form->field($model, 'tanggal',['options' => ['tag' => false]])->textInput(['class'=>'datepicker'])->label(false) ?>

            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Time start</label>
            <div class="col-sm-9"><i class="fa fa-calendar"></i>
            <?= $form->field($model, 'time_start',['options' => ['tag' => false]])->textInput(['class'=>'datetimepicker'])->label(false) ?>

            </div>
        </div>
            <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Time end</label>
            <div class="col-sm-9"><i class="fa fa-calendar"></i>
            <?= $form->field($model, 'time_end',['options' => ['tag' => false]])->textInput(['class'=>'datetimepicker'])->label(false) ?>

            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right">Status aktivasi</label>
            <div class="col-sm-9">
            <?= $form->field($model, 'buka',['options' => ['tag' => false]])->radioList(['Y'=>'Buka','N'=>'Tutup'])->label(false) ?>

            </div>
        </div>
            <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

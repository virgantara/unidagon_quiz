<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Nenad Zivkovic <nenad@freetuts.org>
 * 
 * @since 2.0
 */
class UiiAsset extends AssetBundle
{
	public $basePath = '@webroot';
    public $baseUrl = '@themes';

    public $css = [
        'uii/jquery-ui.css',
        'uii/jquery-ui-timepicker.css',
        'uii/bootstrap.css',
        'uii/bootstrap-theme.css',
        'uii/cropper.css',
        'uii/print.css',
        'uii/template.css'
    ];

    public $js = [
        'uii/jquery.js',
        'uii/jquery-ui.js',
        'uii/jquery-ui-timepicker.js',
        'uii/jquery-validate.js',
        'uii/jquery-redirect.js',
        'uii/bootstrap.js',
        'uii/exis.js',
        'uii/cropper.js',
        'uii/init.js'
        
    ];

    public $depends = [
        'yii\web\YiiAsset',

    ];

}
<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Nenad Zivkovic <nenad@freetuts.org>
 * 
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@themes';

    public $css = [
        'v18/vendor/bootstrap/css/bootstrap.min.css',
        'v18/fonts/font-awesome-4.7.0/css/font-awesome.min.css',
        'v18/fonts/Linearicons-Free-v1.0.0/icon-font.min.css',
        'v18/vendor/animate/animate.css',
        'v18/vendor/css-hamburgers/hamburgers.min.css',
        'v18/vendor/animsition/css/animsition.min.css',
        'v18/vendor/select2/select2.min.css',
        'v18/css/util.css',
        'v18/css/main.css',
        
    ];

    public $js = [
        'v18/vendor/jquery/jquery-3.2.1.min.js',
        'v18/vendor/animsition/js/animsition.min.js',
        'v18/vendor/bootstrap/js/popper.js',
        'v18/vendor/bootstrap/js/bootstrap.min.js',
        'v18/vendor/select2/select2.min.js',
        'v18/js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',

    ];
}

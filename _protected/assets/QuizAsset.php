<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Nenad Zivkovic <nenad@freetuts.org>
 * 
 * @since 2.0
 */
class QuizAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@themes';

    public $css = [
        'klorofil/assets/css/bootstrap.min.css',
        'klorofil/assets/vendor/font-awesome/css/font-awesome.min.css',
        'klorofil/assets/vendor/linearicons/style.css',
        'klorofil/assets/css/source_sans_pro.css',
        'klorofil/assets/css/main.css',

        

    ];

    public $js = [
       
        
    ];


    public $depends = [
        'yii\web\YiiAsset',

    ];
}

<?php
namespace app\helpers;

use Yii;

/**
 * Css helper class.
 */
class MenuHelper
{
    public static function getMenuItems()
    {

    	$userRole = Yii::$app->user->identity->access_role;
        $menuItems = [];
		if(!Yii::$app->user->isGuest){

		     $menuItems[] = [
		        'label' => '<i class="menu-icon fa fa-tachometer"></i><span class="menu-text"> Beranda </span>', 
		        'url' => ['site/index']];
		}

	    // display Users to admin+ roles
	    if (Yii::$app->user->can('admin'))
	    {

	    	 $menuItems[] = [
		        'label' => '<i class="menu-icon fa fa-briefcase"></i><span class="menu-text"> Hasil Ujian </span>', 
		        'url' => ['peserta/hasil']];
	        $menuItems[] = ['label' =>'<i class="menu-icon fa fa-book"></i><span class="menu-text"> Master </span><i class="caret"></i>', 'url' => '#',
	         'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
	         'submenuTemplate' => "\n<ul class='submenu'>\n{items}\n</ul>\n",
	        'items'=>[
	        	[
	                'label' => '<i class="menu-icon fa fa-caret-right"></i>Peserta',  
	                'visible' => Yii::$app->user->can('admin'),
	                'url' => ['peserta/index'],
	                
	            ],
	             [
	            	'label' => '<hr style="padding:0px;margin:0px">'
	            ],
	     		 [
	                'label' => '<i class="menu-icon fa fa-caret-right"></i>Jenis Pertanyaan',  
	                'visible' => Yii::$app->user->can('admin'),
	                'url' => ['jenis-pertanyaan/index'],
	                
	            ],
	            [
	                'label' => '<i class="menu-icon fa fa-caret-right"></i>Sub Bagian',  
	                'visible' => Yii::$app->user->can('admin'),
	                'url' => ['subbagian/index'],
	                
	            ],
	            [
	                'label' => '<i class="menu-icon fa fa-caret-right"></i>Pertanyaan',  
	                'visible' => Yii::$app->user->can('admin'),
	                'url' => ['pertanyaan/index'],
	                
	            ],
	           	[
	                'label' => '<i class="menu-icon fa fa-caret-right"></i>Mapping Pertanyaan',  
	                'visible' => Yii::$app->user->can('admin'),
	                'url' => ['pertanyaan-prodi/index'],
	                
	            ],
	            // [
	            // 	'label' => '<hr style="padding:0px;margin:0px">'
	            // ],
	           
	           	// [
	            //     'label' => '<i class="menu-icon fa fa-caret-right"></i>Syarat Pendaftaran',  
	            //     'visible' => Yii::$app->user->can('admin'),
	            //     'url' => ['syarat/index'],
	                
	            // ],
	            
	        ]];

	       
	    }

	    if (Yii::$app->user->can('theCreator')){
	    	$menuItems[] = [
                'label' => '<i class="menu-icon fa fa-users"></i><span class="menu-text"> Administrator </span><i class="caret"></i>',  
                'submenuTemplate' => "\n<ul class='submenu'>\n{items}\n</ul>\n",
                'visible' => Yii::$app->user->can('admin'),
                'url' => ['#'],
                 'template' => '<a href="{url}" class="dropdown-toggle">{label}</a>',
                'items' => [

                     ['label' => '<i class="menu-icon fa fa-caret-right"></i> Roles ', 'url' => ['/auth-rule/index']],
                     ['label' => '<i class="menu-icon fa fa-caret-right"></i> Auth item', 'url' => ['/auth-item/index']],
                     ['label' => '<i class="menu-icon fa fa-caret-right"></i> Auth item Child', 'url' => ['/auth-item-child/index']],
                     ['label' => '<i class="menu-icon fa fa-caret-right"></i> Assignment ', 'url' => ['/auth-assignment/index']],
                     ['label' => '<i class="menu-icon fa fa-caret-right"></i> Users ', 'url' => ['/user/index']],
                ],
            ];
	    	

	    }

	    if(Yii::$app->user->can('admin')){
	    	$menuItems[] = ['label' => '<i class="menu-icon fa fa-cog"></i><span class="menu-text"> Setting </span>', 'url' => ['/quiz/index']];
	    }


		return $menuItems;
    }
}